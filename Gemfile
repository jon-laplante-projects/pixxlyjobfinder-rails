source "https://rubygems.org"

# git_source(:github) do |repo_name|
#   repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
#   "https://github.com/#{repo_name}.git"
# end


# Bundle edge Rails instead: gem "rails", github: "rails/rails"
gem "rails", "~> 5.0.1"
# Use sqlite3 as the database for Active Record
gem "pg"
# gem "sqlite3"
# Use Puma as the app server
gem "unicorn"
# gem "puma", "~> 3.0"
# Use SCSS for stylesheets"
gem "sass-rails", "~> 5.0"
# Use Uglifier as compressor for JavaScript assets
gem "uglifier", ">= 1.3.0"
# Use CoffeeScript for .coffee assets and views
# gem "coffee-rails", "~> 4.2"
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem "therubyracer", platforms: :ruby

# Use jquery as the JavaScript library
gem "jquery-rails"
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem "turbolinks", "~> 5"
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem "jbuilder", "~> 2.5"
# Use Redis adapter to run Action Cable in production
# gem "redis", "~> 3.0"
# Use ActiveModel has_secure_password
# gem "bcrypt", "~> 3.1.7"

# Use Capistrano for deployment
gem 'capistrano', '~> 3.7', '>= 3.7.1'
gem 'capistrano-rails', '~> 1.2'
gem 'capistrano-passenger', '~> 0.2.0'
gem 'capistrano-rvm'

gem "devise" # Authorization/authentication
gem "bootstrap3-rails" # Add Bootstrap 3 to the asset pipeline.
gem "httparty" # HTTP utility
gem "json" # JSON parsing made easy
gem "nokogiri" # XML parsing made easy
gem "mechanize" # Agent spoofer.
gem "attr_encrypted" # Data encryption.
gem "pry" # Improved CLI.
gem "redd", git: "https://github.com/avinashbot/redd.git" # Reddit API wrapper.
gem "upwork-api" # UpWork API wrapper.
gem "inline_svg" # Use SVG files inline.
gem "oauth" # Ruby oAuth wrapper.
gem "oauth2" # Ruby oAuth2 wrapper.
gem "indeed-ruby" # Ruby wrapper for Indeed.com.
gem "twitter" # Ruby wrapper for Twitter API.
gem 'annotate' # Add a comment summarizing the current schema to the top or bottom of each applicable file.
gem 'omniauth-oauth2', '1.3.1' # Omniauth strategy for generic Oauth2.
gem 'omniauth-twitter' # Twitter omniauth strategy.
gem 'omniauth-upwork' # Upwork omniauth strategy.
gem 'omniauth-reddit', :git => 'git://github.com/jackdempsey/omniauth-reddit.git' # Reddit omniauth strategy.
gem 'kaminari' # Pain free pagination handling.
gem 'whenever' # Pain free CRON job scheduling.
gem 'inherited_resources', '~> 1.7'
gem 'activeadmin', github: 'activeadmin' # Admin backend generator.
gem 'seed_dump' # Generate seed files.

group :development, :test do
  gem "byebug", platform: :mri
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem "listen", "~> 3.0.5"
  gem "web-console"
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem "spring"
  gem "spring-watcher-listen", "~> 2.0.0"
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem "tzinfo-data", platforms: [:mingw, :mswin, :x64_mingw, :jruby]