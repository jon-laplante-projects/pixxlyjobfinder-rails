reddit = ["jonjonjon-throwaway", "jonjonjon-test"]
twitter = ["iwl_research", "jonlaplante"]

listings = Listing.where(["service_id IN (?)", [1,3]])

item_num = 0

listings.each do |l|  
  case l.service_id
    when 1 # Reddit
      l.owner = reddit[item_num]
    
    when 3 # Twitter
      l.owner = twitter[item_num]
  end
  
    l.save
    item_num = item_num === 0 ? 1 : 0
end

puts "Ownership has been assigned to me!"