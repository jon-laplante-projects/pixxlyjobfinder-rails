require 'rubygems'
require 'oauth2'

client = OAuth2::Client.new('client_id', 'client_secret', :site => 'https://pixxlyjobfinder-rails-jonlaplante530707.codeanyapp.com/')

auth_code = client.auth_code.authorize_url(:redirect_uri => 'https://pixxlyjobfinder-rails-jonlaplante530707.codeanyapp.com/')

token = client.auth_code.get_token("#{auth_code}", :redirect_uri => 'https://pixxlyjobfinder-rails-jonlaplante530707.codeanyapp.com/', :headers => {'Authorization' => 'Basic test_password'})

response = token.get('https://oauth.reddit.com/api/v1/me', :params => { 'query_foo' => 'bar' })

response.class.name