require 'rubygems'

require 'indeed-ruby'

client = Indeed::Client.new "8720545778041757"

params = {
    :q => 'logo design',
    :l => 'Remote',
    :userip => '1.2.3.4',
    :useragent => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2)',
}

results = client.search(params)

results["results"].each do |result|
  puts "Title: #{result["jobtitle"]}"
  puts "Desc: #{result["snippet"]}"
  puts "URL: #{result["url"]}"
  puts "Identifier: #{result["jobkey"]}"
  puts "Listed at: #{result["date"]}"
  puts "\n=====================================================\n\n"
end

puts "Done!"