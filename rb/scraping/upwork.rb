require 'rubygems'
require 'json'
require 'httparty'
require 'rss'
# require 'oauth'
require 'upwork/api'
require 'upwork/api/routers/auth'
require 'upwork/api/routers/reports/time'
require 'upwork/api/routers/freelancers/search'

# https://www.upwork.com/ab/feed/jobs/rss?duration=week%2Cnone&q=logo+design&sort=create_time+desc&api_params=1&securityToken=2ddd38c80cdb6fce4b78f190aef885d87ee769feec66e1781744c6fd869821b225336b096b910e1055cfb68b1522f3f739c0370c40598f9cf6096b92128113da&userUid=424136656123445248&orgUid=424136656131833857

# initiate config
config = Upwork::Api::Config.new({
  'consumer_key'    => 'f2922aab2fb84eb75d6e03e0dd63640f',
  'consumer_secret' => '82d432c5f395485a',
#  'access_token'    => '',# assign if known
#  'access_secret'   => '',# assign if known
#  'debug'           => false
})

# setup client
client = Upwork::Api::Client.new(config)

# run authorization in case we haven't done it yet
# and do not have an access token-secret pair
if !config.access_token and !config.access_secret
  authz_url = client.get_authorization_url
  
  # puts "URL: #{authz_url}"
  result = HTTParty.get(authz_url)
  
  # puts "Returned: #{result.body}"
  # puts "Visit the authorization url and provide oauth_verifier for further authorization"
  # puts authz_url
  # verifier = gets.strip
  # @token = client.get_access_token(verifier)
  # store access token data in safe place!
end




# get my auth data
auth = Upwork::Api::Routers::Auth.new(client)




# info = auth.get_user_info

# report = Upwork::Api::Routers::Reports::Time.new(client)
# report_response = report.get_by_freelancer_limited('mnovozhilov', {'tqx' => 'out:json', 'tq' => "select task where worked_on >= '2014-06-01' AND worked_on <= '2014-06-03' order by worked_on"})

# p report_response

params = {'q' => 'python'}
freelancers = Upwork::Api::Routers::Freelancers::Search.new(client)
p freelancers.find(params)


upwork_feed = HTTParty.get("https://www.upwork.com/ab/feed/jobs/rss?duration=week%2Cnone&q=logo+design&sort=create_time+desc&api_params=1&securityToken=2ddd38c80cdb6fce4b78f190aef885d87ee769feec66e1781744c6fd869821b225336b096b910e1055cfb68b1522f3f739c0370c40598f9cf6096b92128113da&userUid=424136656123445248&orgUid=424136656131833857")

upwork_rss = RSS::Parser.parse(upwork_feed.body)

upwork_rss.items.each do |item|
  # title, descr, pubdate, link, guid
  puts "TITLE: #{item.title}"
  puts "Desc: #{item.description}"
  puts "URL: #{item.link}"
  puts "Listed At: #{item.pubDate}"
  puts "Identifier: #{item.guid}"
  puts "\n======================================================================\n\n"
end

puts "Done!"
#    upwork_json = JSON.parse(sub.body)

#       upwork_json["data"]["children"].each do |r|
#         unless r["data"]["title"].downcase.include? "[for hire]"
#           upwork = {}
#           upwork["title"] = r["data"]["title"]
#           upwork["description"] = r["data"]["selftext"]
#           upwork["url"] = r["data"]["url"]
#           upwork["name"] = r["data"]["name"]
#           upwork["permalink"] = r["data"]["permalink"]

#           upwork_listings << upwork

#           puts "#{upwork}"
#           puts "========================================="
#         end
#       end
#     end