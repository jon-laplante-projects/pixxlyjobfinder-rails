require 'rubygems'
require 'nokogiri'
require 'mechanize'
require 'json'

agent = Mechanize.new
agent.user_agent_alias = 'Windows Chrome'

5.times do |count|
  # http://www.guru.com/d/jobs/q/logo/pg/5/
  guru = agent.get("http://www.guru.com/d/jobs/q/logo/#{"pg/".concat(count.to_s) if count > 1}")

  listings = []

  guru.search('.serviceItem').each do |l|
  #   listing = Listing.new
  #   listing.title = "#{l.search(".servTitle>a").text}"
  #   listing.service_id = 5
  #   listing.url = "#{l.attr('href')}"

  #   result = agent.get("#{listing.url}")

  #   listing.descr = "#{result.search('.project-description')}"
  #   listing.identifier = "#{result.search('.info-label').text}"

  #   listed_at = DateTime.now - result.search(".date-to-expire").at(".value").text

  #   listing.listed_at = "#{listed_at}"
  #   listing.save
    listing = {}
    # pph.search('.project-list-item').each do |l|
    listing["title"] = "#{l.search(".servTitle>a").text}"
    listing["url"] = "#{l.search(".servTitle>a").attr('href')}"
    listing["descr"] = "#{l.search("p.desc").text}"
    listing["listed_at"] = "#{l.search(".reltime_new").attr("data-date")}"
  #   listing["identifier"] = "#{l.search(".selectIndivServ>input").value}"
    listing["identifier"] = "#{l.search(".selectIndivServ>input").attr("value")}"
    listings << listing
    
    puts "Title: #{listing["title"]}"
    puts "URL: #{listing["url"]}"
    puts "Desc: #{listing["descr"]}"
    puts "Listed at: #{listing["listed_at"]}"
    puts "Identifier: #{listing["identifier"]}" 
    puts "\n================================================================================\n\n"
  end
end