require 'rubygems'
require 'nokogiri'
require 'mechanize'
require 'json'

agent = Mechanize.new
agent.user_agent_alias = 'Windows Chrome'

pph = agent.get('https://www.peopleperhour.com/freelance-logo-design-jobs')

listings = []

pph.search('.js-paragraph-crop').each do |l|
  listing = Listing.new
  listing.title = "#{l.text}"
  listing.service_id = 6
  listing.url = "#{l.attr('href')}"
  
  result = agent.get("#{listing.url}")
  
  listing.descr = "#{result.search('.project-description')}"
  listing.identifier = "#{result.search('.info-label').text}"
  
  listed_at = DateTime.now - result.search(".date-to-expire").at(".value").text
  
  listing.listed_at = "#{listed_at}"
  listing.save
#   listing = {}
#   # pph.search('.project-list-item').each do |l|
#   listing["text"] = "#{l.text}"
#   listing["href"] = "#{l.attr('href')}"
#   listings << listing
  
# #   puts "Text: #{l.text}"
# #   puts "Hhref: #{l.attr('href')}"
end

# listings.each do |listing|
#   result = agent.get("#{listing['href']}")
  
#   listing["descr"] = result.search(".project-description")
#   listing["expiry_days"] = result.search(".date-to-expire").at(".value").text
#   listing["amount"] = result.search(".price-tag").at("span").text
  
# #   File.open("pph.text", "a") do |f|
# #     f.puts "#{result.body}"
# #     f.puts "=================================================================================="
# #     f.puts "=================================================================================="
# #     f.puts "=================================================================================="
# #   end
# end

listings = Listing.where(service_id: 6)

listings.each do |l|
  puts "Title: #{l.title}"
  puts "Description: #{l.descr}"
  puts "URL: #{l.url}"
  puts "Service: #{l.service}"
  puts "Identifier: #{l.identifier}"
  puts "Listed At: #{l.listed_at}"
  puts " \n========================================\n\n"
end
# listings.each do |l|
#   puts "Text: #{l['text']}"
#   puts "Hhref: #{l['href']}"
#   puts "Desc: #{l['descr']}"
#   puts "Expiry Days: #{l['expiry_days']}"
#   puts "Amount: #{l['amount']}"
# end
# File.open("fl.txt", "w") do |f|
#   freelancer.bases_with(dom_class: 'project-list-item').each do |l|
#     f.puts "Item: #{l.body}"
#   end
# end

puts "Done!"