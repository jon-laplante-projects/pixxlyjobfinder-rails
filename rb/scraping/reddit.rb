require 'rubygems'
require 'nokogiri'
require 'mechanize'
require 'json'

agent = Mechanize.new
agent.user_agent = 'ruby_on_rails.pixxly-job-finder'
    
subreddits = []
    
    hiring = agent.get('https://www.reddit.com/r/hiring/search.json?q=logo+design+%22%5BHiring%5D%22&sort=relevance&restrict_sr=on&t=month')
    designjobs = agent.get('https://www.reddit.com/r/designjobs/search.json?q=logo+design+%22%5BHiring%5D%22&sort=relevance&restrict_sr=on&t=month')

    subreddits << hiring
    subreddits << designjobs
    
    subreddits.each do |sub|
      reddit_listings = []
      # listing = Listing.new

      reddit_json = JSON.parse(sub.body)

      reddit_json["data"]["children"].each do |r|
        unless r["data"]["title"].downcase.include? "[for hire]"
          reddit = {}
          reddit["title"] = r["data"]["title"]
          reddit["description"] = r["data"]["selftext"]
          reddit["url"] = r["data"]["url"]
          reddit["name"] = r["data"]["name"]
          reddit["permalink"] = r["data"]["permalink"]

          reddit_listings << reddit

          puts "#{reddit}"
        #   puts "#{r["data"]["title"]}"
        #   puts "#{r["data"]["selftext"]}"
        #   puts "#{r["data"]["url"]}"
          puts "========================================="
        end
      end
    end