namespace :git do
  desc "Commit changes to Git repository."
  task :commit, [:message] => :environment do |task, args|
    sh %{ git add -A }
    sh %{ git commit -m "#{args.message}" }
    sh %{ git push }
  end
end