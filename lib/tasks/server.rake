namespace :server do
  desc "Start Webrick server daemonized."
  task :startWebrick do
    sh %{ bundle exec rails s -b 0.0.0.0 -d }
  end

  desc "Stop Webrick server daemonized."
  task :stopWebrick do
    sh %{ kill -9 $(cat tmp/pids/server.pid) }
  end

  desc "Ports in use."
  task :ports do
    sh %{ sudo netstat -ntlp | grep LISTEN }
  end
  
  desc "Kill process."
  task :kill, [:message] => :environment do |task, args|
    sh %{ sudo kill -15 args.message }
  end
end
