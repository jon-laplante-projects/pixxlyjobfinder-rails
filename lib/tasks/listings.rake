#############################################
## TODO: Ensure that the same listing is not
## added multiple times.
#############################################

require 'rss'

namespace :listings do
  desc "Collect the Reddit listings."
  task reddit: :environment do
    #############################################
    ## Also do r/designjobs
    ## If title contains "[for hire]", exclude
    #############################################
    agent = Mechanize.new
    agent.user_agent = 'ruby_on_rails.pixxly-job-finder'

    subreddits = []
    
    hiring = agent.get('https://www.reddit.com/r/hiring/search.json?q=logo+design+%22%5BHiring%5D%22&sort=relevance&restrict_sr=on&t=month')
    designjobs = agent.get('https://www.reddit.com/r/DesignJobs/search.json?q=logo+design+%22%5BHiring%5D%22&sort=relevance&restrict_sr=on&t=month')
    # forhire = agent.get('https://www.reddit.com/r/forhire/search.json?q=logo+design+%22%5BHiring%5D%22&sort=relevance&restrict_sr=on&t=month')

    subreddits << hiring
    subreddits << designjobs
    # subreddits << forhire
    
    # reddit_listings = []
    
    subreddits.each do |sub|
      reddit_json = JSON.parse(sub.body)

      reddit_json["data"]["children"].each do |r|
        listing = Listing.new
        reddit = {}
        if r["data"]["title"].downcase.include? "[hiring]"
          listing.service_id = 1
          listing.title = r["data"]["title"]
          # reddit["title"] = r["data"]["title"]
          listing.descr = r["data"]["selftext"]
          # reddit["description"] = r["data"]["selftext"]
          listing.url = r["data"]["url"]
          # listing.url = r["data"]["permalink"]
          # reddit["url"] = r["data"]["url"]
          listing.identifier = r["data"]["name"]
          # reddit["name"] = r["data"]["name"]
          listing.listed_at = r["data"]["created_utc"]
          listing.owner = r["data"]["author"]
          listing.expired_at = Date.today + 30.days

          listing.save
          # reddit_listings << reddit
        end
      end
    end
  end

  desc "Collect the UpWork listings."
  task upwork: :environment do
    # https://www.upwork.com/ab/feed/jobs/rss?duration=week%2Cnone&q=logo+design&sort=create_time+desc&api_params=1&securityToken=2ddd38c80cdb6fce4b78f190aef885d87ee769feec66e1781744c6fd869821b225336b096b910e1055cfb68b1522f3f739c0370c40598f9cf6096b92128113da&userUid=424136656123445248&orgUid=424136656131833857
    upwork_feed = HTTParty.get("https://www.upwork.com/ab/feed/jobs/rss?duration=week%2Cnone&q=logo+design&sort=create_time+desc&api_params=1&securityToken=2ddd38c80cdb6fce4b78f190aef885d87ee769feec66e1781744c6fd869821b225336b096b910e1055cfb68b1522f3f739c0370c40598f9cf6096b92128113da&userUid=424136656123445248&orgUid=424136656131833857")

    upwork_rss = RSS::Parser.parse(upwork_feed.body)

    upwork_rss.items.each do |item|
      listing = Listing.new
      listing.service_id = 2
      listing.title = "#{item.title}"
      listing.descr = "#{item.description}"
      listing.url = "#{item.link}"
      listing.identifier = "#{item.guid}"
      listing.listed_at = "#{item.pubDate}"
      listing.expired_at = Date.today + 30.days
      listing.save
    end
  end

  desc "Collect the Twitter listings."
  task twitter: :environment do
    # https://twitter.com/search?q=looking%20to%20hire
    client = Twitter::REST::Client.new do |config|
      config.consumer_key        = Rails.application.secrets.twitter_key
      config.consumer_secret     = Rails.application.secrets.twitter_secret
    end

    client.search("looking to hire logo design", result_type: "recent").take(100).collect do |tweet|
      listing = Listing.new
      listing.service_id = 3
      listing.title = "Twitter - Logo Design"
      listing.descr = "#{tweet.text}"
      listing.url = "#{tweet.url.to_s}"
      listing.identifier = "#{tweet.id}"
      listing.listed_at = "#{tweet.created_at}"
      listing.owner = "#{tweet.user.screen_name}"
      listing.expired_at = Date.today + 30.days
      listing.save
      # puts "#{tweet.user.screen_name}: #{tweet.text}"
    end
    
    client.search("#freelance logo design", result_type: "recent").take(100).collect do |tweet|
      listing = Listing.new
      listing.service_id = 3
      listing.title = "Twitter#Freelance - Logo Design"
      listing.descr = "#{tweet.text}"
      listing.url = "#{tweet.url.to_s}"
      listing.identifier = "#{tweet.id}"
      listing.listed_at = "#{tweet.created_at}"
      listing.owner = "#{tweet.user.screen_name}"
      listing.expired_at = Date.today + 30.days
      listing.save
    end
  end

  desc "Collect the Guru listings."
  task guru: :environment do
    agent = Mechanize.new
    agent.user_agent_alias = 'Windows Chrome'

    5.times do |count|
      # http://www.guru.com/d/jobs/q/logo/pg/5/
      guru = agent.get("http://www.guru.com/d/jobs/q/logo/#{"pg/".concat(count.to_s) if count > 1}")

      listings = []

      guru.search('.serviceItem').each do |l|
        listing = Listing.new
        listing.title = "#{l.search(".servTitle>a").text}"
        listing.service_id = 5
        listing.url = "http://www.guru.com#{l.search(".servTitle>a").attr('href')}"
        listing.descr = "#{l.search("p.desc").text}"
        listing.identifier = "#{l.search(".selectIndivServ>input").attr("value")}"
        listing.listed_at = "#{l.search(".reltime_new").attr("data-date")}"
        listing.expired_at = Date.today + 30.days
        listing.save
        listing = {}
      end
    end
  end

  desc "Collect the PeoplePerHour listings."
  task peopleperhour: :environment do
    agent = Mechanize.new
    agent.user_agent_alias = 'Windows Chrome'

    5.times do |count|
      pph = agent.get("https://www.peopleperhour.com/freelance-logo-design-jobs#{"?page=".concat(count) if count > 1}")

      pph.search('.js-paragraph-crop').each do |l|
        listing = Listing.new
        listing.title = "#{l.text}"
        listing.service_id = 6
        listing.url = "#{l.attr('href')}"

        result = agent.get("#{listing.url}")

        listing.descr = "#{result.search('.project-description').text}"
        listing.identifier = "#{result.search('li').at('span[title].info-label:not(.text-uppercase)').text}"

        listed_at = DateTime.now - result.search(".date-to-expire").at(".value").text.to_i

        listing.listed_at = "#{listed_at}"
        listing.expired_at = Date.today + 30.days
        listing.save
      end
    end
  end
  
  desc "Collect the Indeed listings."
  task indeed: :environment do
    #############################################
    ## TODO: Introduce required attribution and
    ## click tracking.
    #############################################
    client = Indeed::Client.new "8720545778041757"

    params = {
        :q => 'logo design',
        :l => 'Remote',
        :userip => '1.2.3.4',
        :useragent => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2)',
    }

    results = client.search(params)

    results["results"].each do |result|
      listing = Listing.new
      listing.title = "#{result["jobtitle"]}"
      listing.service_id = 7
      listing.url = "#{result["url"]}"
      listing.descr = "#{result["snippet"]}"
      listing.identifier = "#{result["jobkey"]}"
      listing.listed_at = "#{result["date"]}"
      listing.expired_at = Date.today + 30.days
      listing.save
    end  
  end
  
  desc "Cleaning up old listings."
    task expire_aged: :environment do
      listings = Listing.where(["listed_at < ? AND expired_at IS NULL", Time.now - 30.days])
      
      listings.each do |l|
        l.expired_at = Time.now
        l.save
      end
   end
  
  desc "Reassign listings in dev env to me."
  task own_listings: :environment do
    reddit_arr = ["jonjonjon-throwaway", "jonjonjon-test"]
    twitter_arr = ["iwl_research", "jon_throwaway"]
    
    listings = Listing.all
    
    count = 0
    listings.each do |l|
      case l.service.name
        when "reddit"
          unless reddit_arr.include? l.owner
            l.owner = reddit_arr[count]
          end
        when "twitter"
          unless twitter_arr.include? l.owner
            l.owner = twitter_arr[count]
          end
      end
      
      l.save
      
      count = 1 ? 0 : 1
    end
  end
end