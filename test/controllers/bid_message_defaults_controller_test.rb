# == Schema Information
#
# Table name: bid_message_defaults
#
#  id         :integer          not null, primary key
#  title      :string
#  body       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'test_helper'

class BidMessageDefaultsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @bid_message_default = bid_message_defaults(:one)
  end

  test "should get index" do
    get bid_message_defaults_url
    assert_response :success
  end

  test "should get new" do
    get new_bid_message_default_url
    assert_response :success
  end

  test "should create bid_message_default" do
    assert_difference('BidMessageDefault.count') do
      post bid_message_defaults_url, params: { bid_message_default: { body: @bid_message_default.body, title: @bid_message_default.title } }
    end

    assert_redirected_to bid_message_default_url(BidMessageDefault.last)
  end

  test "should show bid_message_default" do
    get bid_message_default_url(@bid_message_default)
    assert_response :success
  end

  test "should get edit" do
    get edit_bid_message_default_url(@bid_message_default)
    assert_response :success
  end

  test "should update bid_message_default" do
    patch bid_message_default_url(@bid_message_default), params: { bid_message_default: { body: @bid_message_default.body, title: @bid_message_default.title } }
    assert_redirected_to bid_message_default_url(@bid_message_default)
  end

  test "should destroy bid_message_default" do
    assert_difference('BidMessageDefault.count', -1) do
      delete bid_message_default_url(@bid_message_default)
    end

    assert_redirected_to bid_message_defaults_url
  end
end
