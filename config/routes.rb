Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :filter_settings
  resources :user_messages
  resources :bid_message_defaults
  devise_for :users
  resources :listings
  resources :services
  resources :reports
  resources :job_bids
  
  get 'reddit', to: "listings#reddit"
  get 'bid/:range/report', to: "reports#bids"
  
  # get '/auth/:service', to: "services#connect_service_accounts"
  get '/auth/:service/callback', to: "services#connect_service_accounts"
  
  post 'save_credentials', to: "services#create_service_credentials"
  post 'service/accounts/new', to: "services#create_service_accounts"
  
  post 'listing/bookmark', to: "listings#bookmark_toggle"
  post 'listing/filter', to: "listings#listings_search_and_filter"
  
  post 'custom_message', to: "user_messages#create_user_message"
  
  delete 'delete_custom_message', to: "user_messages#delete_user_message"
  
  get 'incoming_messages', to: "incoming_messages#all_incoming"
  
  post 'messages/viewed', to: "incoming_messages#clear_messages"
  post 'message/viewed', to: "incoming_messages#mark_as_viewed"
  
  get 'job_bid_message', to: "job_bids#bid_message"
  get 'all_listings', to: "listings#all_listings"
  
  post 'log_bid', to: "job_bids#log_bid"
  
  # Test the validity of user Oauth tokens.
  post 'token_check', to: "services#check_oauth_tokens"
  
  # Toggle won_at for bids.
  post 'bid/won', to: "job_bids#toggle_won"
  
  get 'bids/today', to: "job_bids#today_bids"
  
  root 'listings#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
