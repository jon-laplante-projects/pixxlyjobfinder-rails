Rails.application.config.middleware.use OmniAuth::Builder do
  provider :developer unless Rails.env.production?
  provider :twitter, "#{Rails.application.secrets.twitter_key}", "#{Rails.application.secrets.twitter_secret}"
  provider :upwork, "#{Rails.application.secrets.upwork_key}", "#{Rails.application.secrets.upwork_secret}"
  provider :reddit, "#{Rails.application.secrets.reddit_key}", "#{Rails.application.secrets.reddit_secret}", scope: "identity edit history mysubreddits privatemessages read", duration: "permanent"
end