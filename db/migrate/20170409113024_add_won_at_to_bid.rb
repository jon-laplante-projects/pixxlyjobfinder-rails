class AddWonAtToBid < ActiveRecord::Migration[5.0]
  def change
    add_column :bids, :won_at, :datetime
  end
end
