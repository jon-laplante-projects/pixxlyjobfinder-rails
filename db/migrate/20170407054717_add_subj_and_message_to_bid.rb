class AddSubjAndMessageToBid < ActiveRecord::Migration[5.0]
  def change
    add_column :bids, :subj, :string
    add_column :bids, :message, :text
  end
end
