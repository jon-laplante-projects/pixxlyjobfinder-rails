class ChangeServiceToServiceIdOnListings < ActiveRecord::Migration[5.0]
  def change
    rename_column :listings, :service, :service_id
  end
end
