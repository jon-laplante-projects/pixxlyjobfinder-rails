class AddPwdIvToServiceKey < ActiveRecord::Migration[5.0]
  def change
    add_column :service_keys, :pwd_iv, :string
  end
end
