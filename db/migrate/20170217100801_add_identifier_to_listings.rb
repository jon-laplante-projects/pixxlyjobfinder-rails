class AddIdentifierToListings < ActiveRecord::Migration[5.0]
  def change
    add_column :listings, :identifier, :string
  end
end
