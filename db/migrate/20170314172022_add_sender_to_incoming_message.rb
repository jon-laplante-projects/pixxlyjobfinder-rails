class AddSenderToIncomingMessage < ActiveRecord::Migration[5.0]
  def change
    add_column :incoming_messages, :sender, :string
  end
end
