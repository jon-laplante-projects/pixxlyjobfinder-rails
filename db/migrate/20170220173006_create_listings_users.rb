class CreateListingsUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :listings_users do |t|
      t.integer :user_id
      t.integer :listing_id

      t.timestamps
    end
  end
end
