class AddListingIdToBid < ActiveRecord::Migration[5.0]
  def change
    add_column :bids, :listing_id, :integer
  end
end
