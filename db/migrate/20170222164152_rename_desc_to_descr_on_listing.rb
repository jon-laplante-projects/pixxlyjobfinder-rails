class RenameDescToDescrOnListing < ActiveRecord::Migration[5.0]
  def change
    rename_column :listings, :desc, :descr
  end
end
