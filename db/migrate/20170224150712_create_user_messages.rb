class CreateUserMessages < ActiveRecord::Migration[5.0]
  def change
    # drop_table :user_messages
    
    create_table :user_messages do |t|
      t.string :title
      t.text :body
      t.integer :bid_message_default_id
      t.integer :user_id

      t.timestamps
    end
  end
end
