class ChangeServiceIdTypeOnListings < ActiveRecord::Migration[5.0]
  def change
    change_column :listings, :service_id, 'integer USING CAST(service_id AS integer)'
  end
end
