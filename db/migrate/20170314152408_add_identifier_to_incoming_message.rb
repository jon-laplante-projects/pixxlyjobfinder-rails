class AddIdentifierToIncomingMessage < ActiveRecord::Migration[5.0]
  def change
    add_column :incoming_messages, :identifier, :string
  end
end
