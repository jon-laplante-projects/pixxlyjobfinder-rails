class AddUidIvToServiceKey < ActiveRecord::Migration[5.0]
  def change
    add_column :service_keys, :uid_iv, :string
  end
end
