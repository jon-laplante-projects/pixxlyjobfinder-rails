class AddTokenSecretToToken < ActiveRecord::Migration[5.0]
  def change
    add_column :tokens, :token_secret, :string
  end
end
