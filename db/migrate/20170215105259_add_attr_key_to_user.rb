class AddAttrKeyToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :attr_key, :string
  end
end
