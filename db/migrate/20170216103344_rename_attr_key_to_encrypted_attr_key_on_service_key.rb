class RenameAttrKeyToEncryptedAttrKeyOnServiceKey < ActiveRecord::Migration[5.0]
  def change
    rename_column :service_keys, :uid, :encrypted_uid
    rename_column :service_keys, :pwd, :encrypted_pwd
    rename_column :service_keys, :uid_iv, :encrypted_uid_iv
    rename_column :service_keys, :pwd_iv, :encrypted_pwd_iv
  end
end
