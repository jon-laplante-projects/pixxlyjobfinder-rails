class CreateFilterSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :filter_settings do |t|
      t.integer :user_id
      t.string :setting
      t.string :val

      t.timestamps
    end
  end
end
