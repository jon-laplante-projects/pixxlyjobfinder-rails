class CreateIncomingMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :incoming_messages do |t|
      t.integer :user_id
      t.integer :service_id
      t.string :title
      t.text :body
      t.boolean :viewed

      t.timestamps
    end
  end
end
