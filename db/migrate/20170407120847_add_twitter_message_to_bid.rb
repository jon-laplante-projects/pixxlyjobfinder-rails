class AddTwitterMessageToBid < ActiveRecord::Migration[5.0]
  def change
    add_column :bids, :twitter_message, :string
  end
end
