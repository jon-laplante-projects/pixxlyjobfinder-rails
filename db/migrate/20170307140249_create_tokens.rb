class CreateTokens < ActiveRecord::Migration[5.0]
  def change
    create_table :tokens do |t|
      t.integer :user_id
      t.integer :service_id
      t.string :auth_code
      t.string :refresh_code
      t.string :token

      t.timestamps
    end
  end
end
