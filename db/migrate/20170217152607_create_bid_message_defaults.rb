class CreateBidMessageDefaults < ActiveRecord::Migration[5.0]
  def change
    create_table :bid_message_defaults do |t|
      t.string :title
      t.text :body

      t.timestamps
    end
  end
end
