class AddMessageIdAndMessageTypeToBid < ActiveRecord::Migration[5.0]
  def change
    add_column :bids, :message_id, :integer
    add_column :bids, :message_type, :string
  end
end
