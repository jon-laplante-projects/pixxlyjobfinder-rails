class CreateServiceKeys < ActiveRecord::Migration[5.0]
  def change
    create_table :service_keys do |t|
      t.integer :user_id
      t.integer :service_id
      t.string :uid
      t.string :pwd

      t.timestamps
    end
  end
end
