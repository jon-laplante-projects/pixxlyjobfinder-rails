class CreateListings < ActiveRecord::Migration[5.0]
  def change
    create_table :listings do |t|
      t.string :service
      t.string :title
      t.text :desc
      t.string :url
      t.datetime :listed_at

      t.timestamps
    end
  end
end
