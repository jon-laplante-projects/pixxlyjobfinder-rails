class AddVerifierToToken < ActiveRecord::Migration[5.0]
  def change
    add_column :tokens, :verifier, :string
  end
end
