user_lists = current_user.listings.map { |l| l.id }

json.extract! listing, :id, :service_id, :title, :descr, :url, :listed_at, :created_at, :updated_at
json.bookmark user_lists.include? listing.id
json.service listing.service.name
json.bulkreply listing.service.bulkreply
json.url listing_url(listing, format: :json)