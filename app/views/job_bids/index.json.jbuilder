json.bids @bids.each do |bid|
  json.bid_at bid.bid_at
  json.service bid.listing.service.name
  json.job_name bid.listing.title
  json.job_description bid.listing.descr
  json.won_at bid.won_at
end