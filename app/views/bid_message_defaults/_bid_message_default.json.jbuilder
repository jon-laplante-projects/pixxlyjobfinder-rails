json.extract! bid_message_default, :id, :title, :body, :created_at, :updated_at
json.url bid_message_default_url(bid_message_default, format: :json)