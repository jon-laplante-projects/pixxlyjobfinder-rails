class ApplicationController < ActionController::Base
  # protect_from_forgery with: :exception
  before_action :set_current_user
  
  layout :layout_by_resource
  
  def layout_by_resource
    if devise_controller? && resource_name == :user && action_name == "new" || action_name == "edit"
      false
    else
      nil
    end
  end
  
  def set_current_user
    if current_user
      ServiceKey.attr_key = current_user.attr_key
    end
  end
end