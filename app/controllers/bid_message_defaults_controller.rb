class BidMessageDefaultsController < ApplicationController
  before_action :set_bid_message_default, only: [:show, :edit, :update, :destroy]

  # GET /bid_message_defaults
  # GET /bid_message_defaults.json
  def index
    @bid_message_defaults = BidMessageDefault.all
  end

  # GET /bid_message_defaults/1
  # GET /bid_message_defaults/1.json
  def show
    unless params[:id].blank?
      @message = BidMessageDefault.find(params[:id])
      @message_id = params[:message_id]
            
      respond_to do |format|
        format.js {}
        format.html {}
      end
    end
  end

  # GET /bid_message_defaults/new
  def new
    @bid_message_default = BidMessageDefault.new
  end

  # GET /bid_message_defaults/1/edit
  def edit
  end

  # POST /bid_message_defaults
  # POST /bid_message_defaults.json
  def create
    @bid_message_default = BidMessageDefault.new(bid_message_default_params)

    respond_to do |format|
      if @bid_message_default.save
        format.html { redirect_to @bid_message_default, notice: 'Bid message default was successfully created.' }
        format.json { render :show, status: :created, location: @bid_message_default }
      else
        format.html { render :new }
        format.json { render json: @bid_message_default.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bid_message_defaults/1
  # PATCH/PUT /bid_message_defaults/1.json
  def update
    respond_to do |format|
      if @bid_message_default.update(bid_message_default_params)
        format.html { redirect_to @bid_message_default, notice: 'Bid message default was successfully updated.' }
        format.json { render :show, status: :ok, location: @bid_message_default }
      else
        format.html { render :edit }
        format.json { render json: @bid_message_default.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bid_message_defaults/1
  # DELETE /bid_message_defaults/1.json
  def destroy
    @bid_message_default.destroy
    respond_to do |format|
      format.html { redirect_to bid_message_defaults_url, notice: 'Bid message default was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bid_message_default
      @bid_message_default = BidMessageDefault.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bid_message_default_params
      params.require(:bid_message_default).permit(:title, :body)
    end
end
