class ServicesController < ApplicationController
  def create_service_credentials
    unless params[:username].blank? || params[:password].blank?
      begin
        client = RedditKit::Client.new
        client.api_endpoint = "https://www.reddit.com"
        client.sign_in("#{params[:username]}", "#{params[:password]}")
      rescue
        render json: { success: false, message: "Cannot sign in with provided credentials." }, status: 200
        return
      end
      
      if client.signed_in?
        service = Service.find_by_name("reddit")
        
        credentials = ServiceKey.new
        credentials.user_id = current_user.id
        credentials.service_id = service.id
        credentials.uid = "#{params[:username]}"
        credentials.pwd = "#{params[:password]}"
        credentials.save
        
        render json: { success: true }, status: 200
      end
    else
      render json: { success: false, message: "Username or password not provided." }, status: 200
    end
  end
  
  def connect_service_accounts
    # OAuth endpoint.
    # uid = params[:state].split("_")[0]
    service = params[:service]
#     service = params[:state].split("_")[1]
    
    case service
      when "rdt", "reddit"
        service_id = Service.select("id").find_by(name: "reddit")
        token = current_user.tokens.new
        token.token = request.env["omniauth.auth"]["credentials"]["token"]
        token.refresh_code = request.env["omniauth.auth"]["credentials"]["refresh_token"]
        token.service_id = service_id.id
        token.auth_code = params[:code]
        token.save
      when "twitter"
        service_id = Service.select("id").find_by(name: "twitter")
        token = current_user.tokens.new
        token.service_id = service_id.id
        token.token = request.env["omniauth.auth"]["extra"]["access_token"].token
        token.token_secret = request.env["omniauth.auth"]["extra"]["access_token"].secret
        token.auth_code = params[:oauth_token]
        token.verifier = params[:oauth_verifier]
        token.save
      when "upwork"
        service_id = Service.select("id").find_by(name: "upwork")
        token = current_user.tokens.new
        token.service_id = service_id.id
        token.token = request.env["omniauth.auth"]["credentials"].token
        token.token_secret = request.env["omniauth.auth"]["credentials"].secret
        token.auth_code = params[:oauth_token]
        token.verifier = params[:oauth_verifier]
        token.save
    end
    
    redirect_to "/"
  end
  
  def create_service_accounts
    # Get the list of accounts to create + credentials.Get
    unless params[:services].blank? || params[:uid].blank? || params[:mail].blank? || params[:pwd].blank?
      params[:services].each do |s|
        case s
          when "reddit"
            # ERR - results: {"json": {"errors": [["USERNAME_TAKEN", "that username is already taken", "user"]]}}   
            response = HTTParty.post("https://www.reddit.com/api/register/#{params[:uid]}", :body => {op: "reg", user: "#{params[:uid]}", passwd: "#{params[:pwd]}", passwd2: "#{params[:pwd]}", email: "#{params[:mail]}", api_type: "json"}, headers: {"User-Agent" => "ruby_on_rails.pixxly-job-finder"})
            puts "Service is Reddit."
            puts "Account creation results: #{response.body}"
            puts "=========================================================================="
          when "twitter"
            puts "Service is Twitter."
          when "upwork"
            puts "Service is UpWork."
          when "guru"
            puts "Service is Guru."
          when "freelancer"
            puts "Service is Freelancer."
          when "indeed"
          
        end
      end
      render json: { success: true }, status: 200
    else
      render json: { success: false, message: "Services, email, or password not provided." }, status: 200
    end
    # Create accounts.
    
    # If necessary, get/create API key(s).
    
    # Persist account details to data store.
    
  end
  
  def check_oauth_tokens
    tokens = current_user.tokens
    tokens_valid = true
    
    tokens.each do |token|
      case token.service.name
        when "reddit"
          web = Redd::AuthStrategies::Web.new(user_agent: 'PixxlyJobFinder:v1.3.0',
            client_id:  Rails.application.secrets.reddit_key,
            secret:     Rails.application.secrets.reddit_secret,
            redirect_uri: Rails.application.secrets.callback_uri)

          client = Redd::APIClient.new(web)
        
          begin
            client.access = Redd::Models::Access.new(web, access_token: token.token)
            # client.authenticate(token.auth_code)
          rescue => e
            if e.message.to_s === "{\"error\": \"invalid_grant\"}"
              new_access = { access_token: token.token, refresh_token: token.refresh_code, expires_in: 9999 }
              client.access = Redd::Models::Access.new(web, new_access)
              # client.refresh(token.refresh_code)
            end

            tokens_valid = false
          end
        
        when "twitter"
          client = Twitter::REST::Client.new do |config|
            config.consumer_key        = "#{Rails.application.secrets.twitter_key}"
            config.consumer_secret     = "#{Rails.application.secrets.twitter_secret}"
            config.access_token        = "#{token.token}"
            config.access_token_secret = "#{token.token_secret}"
          end
        
          begin
            client.search("test", result_type: "recent").take(10).collect
          rescue => e
            logger.info "Error message: #{e.message}"
            token.destroy
            tokens_valid = false
          end
      end
    end
      
      render json: { valid: tokens_valid }
  end
end