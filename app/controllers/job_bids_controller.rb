require "csv"

class JobBidsController < ApplicationController
  def index
    ###########################################
    # Application nav data.
    ###########################################
    # User bids.
    @user_bids = Bid.where(user_id: current_user.id).pluck(:listing_id)
    
    # Default message templates.
    @templates = BidMessageDefault.all
    winning = current_user.bids.where("won_at IS NOT NULL").group(:message_type).calculate(:count, :message_id)
    
    @best_msg = {}

    unless winning.blank?
      if (winning["user"] > winning["template"] || winning["user"] === winning["template"] && winning["user"]) && winning["user"] != nil
        # User generated is most popular or there is a tie between most popular user and default templates.
        winner_id = current_user.bids.where(message_type: "user").group(:message_id).calculate(:maximum, :message_id)
        @best_msg["type"] = "user"
        @best_msg["id"] = winner_id.first[0]
      elsif winning["user"] === 0 && winning["template"] === "0"
        # No data.
        @best_msg["type"] = "user"
        @best_msg["id"] = 0
      else
        # Template is most popular.
        winner_id = current_user.bids.where(message_type: "template").group(:message_id).calculate(:maximum, :message_id)
        @best_msg["type"] = "template"
        @best_msg["id"] = winner_id.first[0]
      end
    end
    
    # User-made message templates.
    @user_templates = current_user.user_messages
    
    # Check to see if there are messages for the user.
    msg = IncomingMessage.new
    msg.check(1, current_user.id)
    msg.check(3, current_user.id)
    
    # Replies received from bulk-ready providers.
    @incoming_messages = current_user.incoming_messages.where(viewed: nil)
    
    @tokens = Token.where(user_id: current_user.id).pluck(:service_id)
    @reddit_messages = current_user.incoming_messages.where(service_id: 1, viewed: nil)
    @twitter_messages = current_user.incoming_messages.where(service_id: 3, viewed: nil)
    ###########################################
    # End application nav data.
    ###########################################
    
    @bids = current_user.bids.order("bid_at DESC")
    
    @bid_duration = params[:duration] || "all"
    case @bid_duration
        when "all"
          @bids = current_user.bids.order("bid_at DESC")
        when "30_days"
          @bids = current_user.bids.where(["bid_at > ?", Time.now - 30.days]).order("bid_at DESC")
        when "90_days"
          @bids = current_user.bids.where(["bid_at > ?", Time.now - 90.days]).order("bid_at DESC")
        when "6_months"
          @bids = current_user.bids.where(["bid_at > ?", Time.now - 6.months]).order("bid_at DESC")
    end
 
    # Build the CSV file.
    bid_csv = CSV.generate do |csv|
      csv << ["bid_at", "service", "job_name", "job_description", "won_at"]
      
      @bids.each do |bid|
        csv << ["#{bid.bid_at}", "#{bid.listing.service.name}", "#{bid.listing.title}", "#{bid.listing.descr}", "#{bid.won_at}"]
      end
    end
    
    # Stats
    @added_today = Listing.where("created_at >= ?", Time.zone.now.beginning_of_day).count
    @currently_available = Listing.where("expired_at >= ?", Time.now).count
    @processed = Listing.count

    respond_to do |format|
      format.html {  }
      format.js {  }
      format.json {  }
      format.csv { send_data bid_csv, filename: "job-bids-#{Date.today}.csv" }
    end
  end
  
  def log_bid
    # NO VIEW - AJAX only, for logging purposes.
    if params[:listing_id].blank? === false && params[:message].blank? === false # If we've got both a listing ID and a message, we're bidding on a single listing.
      bid = current_user.bids.new
      bid.listing_id = params[:listing_id]
      bid.message_type = params[:message_type]
      
      unless params[:message_id].blank?
        bid.message_id = params[:message_id]
      end
      
      unless params[:subj].blank?
        bid.subj = params[:subj]
      end
      
      unless params[:twitter].blank?
        bid.twitter_message = params[:twitter]
      end
      
      bid.message = params[:message]
      
      bid.bid_at = Time.now
      bid.save
      
      render json: { success: true }, status: 200 and return
      
    elsif params[:message] && params[:listing_id].blank? # This seems to be a case where bulk applying is happening.
      bids = current_user.bids.pluck(:listing_id)
      listings = if bids.count > 0
        current_user.listings.where(["listing_id NOT IN (?)", bids])
      else
        current_user.listings
      end
            
      listings.each do |l|
        if l.service.bulkreply # Only send a bid if this is a bulk apply service.
          bid = current_user.bids.new
          bid.listing_id = l.id

          unless params[:subj].blank?
            bid.subj = params[:subj]
          end

          unless params[:twitter].blank?
            bid.twitter_message = params[:twitter]
          end

          bid.message = params[:message]

          bid.bid_at = Time.now
          bid.save
          
          puts "Bid saved."
        end
      end
      
      render json: { success: true }, status: 200 and return
    else # We don't have all the params we need to do anything meaningful. Send back an error.
        render json: { success: false, message: "Required parameters are missing." }, status: 200 and return
    end
  end
  
  def job_bid_report(length)
    # Will probably need to use SQL to do a join to properly display all the data together.
    case length
      when "all"
        listing_ids = ListingsUser.where(user_id: current_user.id).pluck(:listing_id)
      
      when "30"
        listing_ids = ListingsUser.where(user_id: current_user.id).pluck(:listing_id)
      
      else
      # Doesn't match the others.
    end
    
    listings = Listing.where(["id in (?)", listing_ids])
  end
  
  def toggle_won
    unless params[:bid_id].blank?
      bid = current_user.bids.find(params[:bid_id])
      
      if bid.won_at.blank?
        bid.won_at = Time.now
        won_at = Time.now.strftime("%B %e, %Y")
        toggle = "on"
      else
        toggle = "off"
        won_at = ""
        bid.won_at = nil
      end
      
      bid.save
      
      render json: { success: true, won_at: won_at, toggle: toggle }, status: 200
    else
      render json: { success: false, message: "Cannot update. No bid ID provided." }, status: 200
    end
  end
  
  def today_bids
    bid_ids = current_user.bids.where(["bid_at > ?", Date.today - 1]).pluck(:listing_id)
    
    render json: bid_ids.to_json
  end

  # DELETE /listings/1
  # DELETE /listings/1.json
  def destroy
    bid = Bid.find(params[:id])
    bid.destroy
    
    respond_to do |format|
      format.html { redirect_to job_bids_url, notice: 'Bid was successfully destroyed.' }
      format.js { redirect_to job_bids_url }
      format.json { head :no_content }
    end
  end
end