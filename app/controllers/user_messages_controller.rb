class UserMessagesController < ApplicationController
  before_action :set_user_message, only: [:show, :edit, :update, :destroy]

  # GET /user_messages
  # GET /user_messages.json
  def index
    @user_messages = UserMessage.all
  end

  # GET /user_messages/1
  # GET /user_messages/1.json
  def show
    unless params[:id].blank?
      @message = UserMessage.find(params[:id])
      @message_id = params[:message_id]

      respond_to do |format|
        format.js {}
        format.html {}
      end
    end
  end

  # GET /user_messages/new
  def new
    @user_message = UserMessage.new
  end

  # GET /user_messages/1/edit
  def edit
  end

  # POST /user_messages
  # POST /user_messages.json
  def create
    @user_message = UserMessage.new(user_message_params)

    respond_to do |format|
      if @user_message.save
        format.html { redirect_to @user_message, notice: 'User message was successfully created.' }
        format.json { render :show, status: :created, location: @user_message }
      else
        format.html { render :new }
        format.json { render json: @user_message.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /user_messages/1
  # PATCH/PUT /user_messages/1.json
  def update
    respond_to do |format|
      if @user_message.update(user_message_params)
        format.html { redirect_to @user_message, notice: 'User message was successfully updated.' }
        format.json { render :show, status: :ok, location: @user_message }
      else
        format.html { render :edit }
        format.json { render json: @user_message.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user_messages/1
  # DELETE /user_messages/1.json
  def destroy
    @user_message.destroy
    respond_to do |format|
      format.html { redirect_to user_messages_url, notice: 'User message was successfully destroyed.' }
      format.json { head :no_content }
    end
    
    redirect_to :root_path
  end
  
  def create_user_message
    unless params[:title].blank? || params[:body].blank?
      # Eval the params to determine if this is a create or edit action.
      case "#{params[:mode]}"
        when "new"
          msg = UserMessage.new
        when "edit"
          unless params[:template_id].blank?
            puts "Finding a rec..."
            msg = UserMessage.find(params[:template_id])
          else
            msg = UserMessage.new
          end
        else
          msg = UserMessage.new
      end
      
      msg.title = params[:title]
      msg.body = params[:body]
      unless params[:template_id].blank? || params[:mode] === "edit"
        msg.bid_message_default_id = params[:template_id]
      end
      msg.user_id = current_user.id
      msg.save
      
      render json: { success: true, custom_msg: msg, msg_truncated_body: msg.body.truncate(200), id: msg.id }, status: 200
    else
      render json: { success: false, message: "Please provide required parameters." }, status: 200
    end
  end
  
  def delete_user_message
    unless params[:id].blank?
      del = UserMessage.find(params[:id])
      
      unless del.blank?
        del.destroy
        del.save
      end
      
      msg = current_user.user_messages
      render json: { success: true, custom_msg: msg }, status: 200
    else
      render json: { success: false, message: "Please provide required parameters." }, status: 200
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_message
      @user_message = UserMessage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_message_params
      params.require(:user_message).permit(:title, :body, :bid_message_default_id)
    end
end
