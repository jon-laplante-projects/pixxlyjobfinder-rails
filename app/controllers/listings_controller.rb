require "csv"

class ListingsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_listing, only: [:show, :edit, :update, :destroy]

  # GET /listings
  # GET /listings.json
  def index
    @sort_by = params[:sort_col]
    @sort_direction = params[:sort_direction]
    
    page_num = !params[:page].blank? ? params[:page] : 1
    # page_num = params[:page] || 1
    
    filters = current_user.filter_settings.all
    
    settings = {}
    filters.each do |f|
      settings["#{f.setting}"] = "#{f.val}"
    end

    # List of the available filter params.
    setting_names = ["search", "title", "post", "offset", "bookmarks", "services", "bulkreply"]

    setting_names.each do |s|
      if params[s.to_sym].blank?
        # Test to see if we are clearing the search text.
        unless params[:clear_search].to_s === "true" && s === "search"
          # We're NOT resetting the search text, just getting the param value.
          params[s.to_sym] = settings[s.to_s]
        else
          # If we're resetting the search text.
          params[:search] = ""
          
          # Persist the reset.
          if current_user.filter_settings.where(setting: "#{s}").count > 0
            filter = current_user.filter_settings.find_by(setting: "#{s}")
          else
            filter = current_user.filter_settings.new
          end
          
          filter.setting = "search"
          filter.val = nil
          filter.save
        end
      else
        if current_user.filter_settings.where(setting: "#{s}").count > 0
          filter = current_user.filter_settings.find_by(setting: "#{s}")
        else
          filter = current_user.filter_settings.new
        end
        # Populate/update the rec and save.
        filter.setting = s
        unless s === "services"
          eval("filter.val = params[:#{s}]")
        else
          unless params[:services].blank?
            if params[:services][0] === "["
              filter.val = eval(params[:services])
            else
              filter.val = params[:services].split(",")
            end
          end
          
          # services = params[:services].split(",")
          # filter.val = services
        end          
        filter.save
      end
    end
    
    @settings = params

    # Inconsistencies in persisting and retrieving the array, so we'll look at several factors and convert based on results.
#     unless @settings[:services].class.to_s === "Array"
#       unless @settings[:services].blank?
#         if @settings[:services][0] === "["
#           @settings[:services] = eval(@settings[:services])
#         else
#           @settings[:services] = @settings[:services].split(",")
#         end
#       end
#     end

    unless params.blank?
      @listings = filter(params)
    else
      @listings = @listings.page(page_num).order("id DESC")
    end
    
    # User bids.
    @user_bids = Bid.where(user_id: current_user.id).pluck(:listing_id)
    
    # Default message templates.
    @templates = BidMessageDefault.all
    winning = current_user.bids.where("won_at IS NOT NULL").group(:message_type).calculate(:count, :message_id)
    
    @best_msg = {}
    
    unless winning.blank?
      if (winning["user"] > winning["template"] || winning["user"] === winning["template"] && winning["user"]) && winning["user"] != nil
        # User generated is most popular or there is a tie between most popular user and default templates.
        winner_id = current_user.bids.where(message_type: "user").group(:message_id).calculate(:maximum, :message_id)
        @best_msg["type"] = "user"
        @best_msg["id"] = winner_id.first[0]
      elsif winning["user"] === 0 && winning["template"] === "0"
        # No data.
        @best_msg["type"] = "user"
        @best_msg["id"] = 0
      else
        # Template is most popular.
        winner_id = current_user.bids.where(message_type: "template").group(:message_id).calculate(:maximum, :message_id)
        @best_msg["type"] = "template"
        @best_msg["id"] = winner_id.first[0]
      end
    end
    
    # User-made message templates.
    @user_templates = current_user.user_messages
    
    # Replies received from bulk-ready providers.
    @incoming_messages = current_user.incoming_messages.where(viewed: nil)
    
    # Check to see if there are messages for the user.
    msg = IncomingMessage.new
    msg.check(1, current_user.id)
    msg.check(3, current_user.id)
    
    @reddit_messages = current_user.incoming_messages.where(service_id: 1, viewed: nil).order("id DESC")
    @twitter_messages = current_user.incoming_messages.where(service_id: 3, viewed: nil).order("id DESC")
    @tokens = Token.where(user_id: current_user.id).pluck(:service_id)
    @filtered_count = @listings.total_count
    
    # Stats
    @added_today = Listing.where("created_at >= ?", Time.zone.now.beginning_of_day).count
    @currently_available = Listing.where("expired_at >= ?", Time.now).count
    @processed = Listing.count

    respond_to do |format|
      format.html { }
      format.js { }
      format.json {  }
      format.csv { send_data @listings.to_csv, filename: "listings-#{Date.today}-page#{page_num}.csv" }
    end
  end

  # GET /listings/1
  # GET /listings/1.json
  def show
  end

  # GET /listings/new
  def new
    @listing = Listing.new
  end

  # GET /listings/1/edit
  def edit
  end

  # POST /listings
  # POST /listings.json
  def create
    @listing = Listing.new(listing_params)

    respond_to do |format|
      if @listing.save
        format.html { redirect_to @listing, notice: 'Listing was successfully created.' }
        format.json { render :show, status: :created, location: @listing }
      else
        format.html { render :new }
        format.json { render json: @listing.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /listings/1
  # PATCH/PUT /listings/1.json
  def update
    respond_to do |format|
      if @listing.update(listing_params)
        format.html { redirect_to @listing, notice: 'Listing was successfully updated.' }
        format.json { render :show, status: :ok, location: @listing }
      else
        format.html { render :edit }
        format.json { render json: @listing.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /listings/1
  # DELETE /listings/1.json
  def destroy
    @listing.destroy
    respond_to do |format|
      format.html { redirect_to listings_url, notice: 'Listing was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  #=============================================
  # Get listings
  #=============================================
  ##############################################
  ## TODOS
  ## [] Don't forget to add time constraints
  ##############################################
  def bookmark_toggle
    unless params[:listing_id].blank? || params[:status].blank?
      bookmark = ListingsUser.find_by(listing_id: params[:listing_id])
      
      if params[:status] == "false"
        # If we have a record, retrieve it.
        unless bookmark.blank?
          bookmark.destroy
        end  
      else
        # Don't create a new bookmark for this user/listing if one exists.
        if bookmark.blank?
          bookmark = ListingsUser.new
          bookmark.user_id = current_user.id
          bookmark.listing_id = params[:listing_id]
          bookmark.save
        end
      end
      render json: { success: true }, status: 200
    else
      render json: { success: false, message: "Please provide required parameters." }, status: 200
    end
  end
  
  def add_bid
    unless params[:listing_id].blank?
      bid = Bid.find_by(listing_id: params[:listing_id])
      
      render json: { success: true }, status: 200
    else
      render json: { success: false, message: "Please provide required parameters." }, status: 200
    end
  end
  
  def filter(params)
    queries = []
    query = ""
    bookmarks_query = ""
    bookmarks_count = current_user.listings.where(expired_at: nil).count
    services_query = ""
    page_num = params[:page] || 1
    
    if params[:services].class.to_s === "String"
      if params[:services][0] === "["
        service_arr = eval(params[:services])
      else
        service_arr = params[:services].split(",")
      end
    else
      service_arr = params[:services]
    end

    # build the services query
    unless service_arr.blank?
      services = service_arr.map { |s| "'#{s}'" }
      
      case params[:bulkreply]
        when "bulk"
          service_ids = Service.where("name in (#{services.join(",")}) AND bulkreply IS TRUE").pluck(:id)
          services_query = "service_id IN (#{service_ids.join(",")})"

        when "no_bulk"
          service_ids = Service.where("name in (#{services.join(",")}) AND bulkreply IS FALSE").pluck(:id)
          services_query = "service_id IN (#{service_ids.join(",")})"
        
        else
          service_ids = Service.where("name in (#{services.join(",")})").pluck(:id)
          services_query = "service_id IN (#{service_ids.join(",")})"
      end
    end

    # build title search query
    if params[:title].to_s === "true"
      queries <<  "title LIKE '%#{params[:search]}%'"
      # title_query = " OR title LIKE '%#{params[:search]}%'"
    end

    # build the post search query
    if params[:post].to_s === "true"
      queries << "descr LIKE '%#{params[:search]}%'"
      # post_query = " OR desc LIKE '%#{params[:search]}%'"
    end

    # build the bookmarks query
    unless params[:bookmarks].blank?
      if params[:bookmarks].to_s === "true"# && bookmarks_count > 0
         unless bookmarks_count === 0
          bookmarked = current_user.listings
          bookmarked_ids = []

          bookmarked.each do |b|
            bookmarked_ids << b.id  
          end

          # queries << "id IN (#{bookmarked_ids.join(",")})"
          bookmarks_query = "id IN (#{bookmarked_ids.join(",")})"
         else  
           return Listing.none
         end
      end
    end

  if queries.count > 0
    # Bookmarks and bulkreplies should be 'AND' rather than 'OR'
    close = queries.join(" OR ")
    close = "(#{close})"
    # close = queries.drop(1).join(" OR ")
  end
    
  if !bookmarks_query.blank? && !services_query.blank?
    condition_queries = bookmarks_query.concat(" AND #{services_query}")
  elsif bookmarks_query.blank? && !services_query.blank?
    condition_queries = services_query
  elsif !bookmarks_query.blank? && services_query.blank?
    condition_queries = bookmarks_query
  else
    condition_queries = ""
  end
  
  # Bookmarks, too?
  unless condition_queries.blank?
    unless close.blank?
      close.concat(" AND #{condition_queries}")
    else
      close = condition_queries
    end
  end
  ###################################

    # Set sort preferences, if they exist.
    case params[:sort_col]
      when nil, "undefined"
        order_by = "id"
      
      when "service"
        order_by = "services.name"
      
    else
      order_by = params[:sort_col]
    end

    # Set the sort direction, based on params.
    order_direction = !params[:sort_direction].nil? ? params[:sort_direction] : "DESC"
    order_direction = "DESC" if order_direction === "undefined"

    order_str = "#{order_by} #{order_direction}"
    
    # Build the query.
    unless queries.blank?
      unless close.blank?
        query = "Listing.includes(\"service\").where([\"#{close} AND expired_at >= ?\",Time.now]).page(#{page_num.to_i}).order('#{order_str}')"  
      else
        query = "Listing.includes(\"service\")where([\"#{queries.first} AND expired_at >= ?\",Time.now]).page(#{page_num.to_i}).order('#{order_str}')"
      end
    else
     unless close.blank?
        query = "Listing.where([\"#{close} AND expired_at >= ?\",Time.now]).page(#{page_num.to_i}).order('#{order_str}')"
      else
        query = "Listing.where([\"expired_at >= ?\",Time.now]).page(#{page_num.to_i}).order('#{order_str}')"
      end
    end

    return eval(query)
  end
  
  def listings_search_and_filter
    unless params[:title].blank? || params[:post].blank? || params[:offset].blank?
      
      settings = ["search", "title", "post", "offset", "bookmarks", "services", "bulkreply"]
      
      settings.each do |s|
        unless params[s.to_sym].blank?
          
          if FilterSetting.where(user_id: current_user.id, setting: s).count > 0
            filter = FilterSetting.find_by(user_id: current_user.id, setting: s)
          else
            filter = current_user.filter_settings.new
          end
          
          filter.setting = s
          filter.val = params[s.to_s]
          filter.save
        end
      end
      
      results = filter(params)
      # results = Listing.find_by_sql("SELECT * FROM listings WHERE id IS NOT NULL#{title_query}#{post_query}#{services_query}#{bookmarks_query} ORDER BY created_at DESC;")
      
      render json: { success: true, listings: results }, status: 200
    else
      render json: { success: false, message: "Please provide required parameters." }, status: 200
    end
  end
  
  def all_listings
    listings = Listing.all.order('id DESC')
    
    respond_to do |format|
      format.csv{ send_data listings.to_csv, filename: "listings-#{Date.today}-all.csv" }
    end
  end
  
  def hundred_listings
    
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_listing
      @listing = Listing.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def listing_params
      params.require(:listing).permit(:service, :title, :descr, :url, :listed_at)
    end
end
