class IncomingMessagesController < ApplicationController
  def all_incoming
    # Check to see if there are messages for the user.
    msg = IncomingMessage.new
    msg.check(1, current_user.id)
    msg.check(3, current_user.id)
    
    # Get the current messages.
    messages = current_user.incoming_messages.where("viewed IS NULL or viewed = false").includes(:service)

    msg_arr = []

    # {\"id\":2,\"user_id\":2,\"service_id\":1,\"title\":\"re: This is a test of the API.\",\"body\":\"Okay - so...do we have this figured out now, or what?\",\"viewed\":null,\"created_at\":\"2017-03-14T16:14:43.470Z\",\"updated_at\":\"2017-03-14T16:14:43.470Z\",\"identifier\":\"t4_7suif6\"}
    messages.each do |m|
      msg_hash = {}
      msg_hash["id"] = m.id
      msg_hash["service"] = "#{m.service.name}"
      msg_hash["title"] = "#{m.title}"
      msg_hash["body"] = "#{m.body}"
      msg_hash["sender"] = "#{m.sender}"
      msg_hash["viewed"] = m.viewed
      msg_hash["created_at"] = "#{m.created_at}"
      
      msg_arr << msg_hash
    end
    
    render json: msg_arr, status: 200
    #respond_to do |format|
      #format.json { render messages.to_json }
    #end
  end
  
  def clear_messages
    unless params[:service].blank? || current_user.id.blank?
      service_id = Service.find_by(name: params[:service])
      messages = current_user.incoming_messages.where(service_id: service_id)
      
      messages.each do |msg|
        msg.viewed = true
        msg.save
      end
      
      render json: { success: true }, status: 200 and return
    else
      render json: { success: false, message: "Required parameters are missing." }, status: 200 and return
    end
  end
  
  def mark_as_viewed
    unless params[:message_id].blank?
      msg = IncomingMessage.find(params[:message_id])
      
      unless msg.blank?
        msg.viewed = true
        msg.save
        
        render json: { success: true }, status: 200 and return
      else
        render json: { success: false, message: "No message to update." }, status: 200 and return
      end
    else
      render json: { success: false, message: "Required paramters are missing." }, status: 200 and return
    end
  end
end