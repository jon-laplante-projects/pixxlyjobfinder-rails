# == Schema Information
#
# Table name: filter_settings
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  setting    :string
#  val        :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class FilterSetting < ApplicationRecord
  belongs_to :user
end
