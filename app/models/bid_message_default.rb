# == Schema Information
#
# Table name: bid_message_defaults
#
#  id         :integer          not null, primary key
#  title      :string
#  body       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class BidMessageDefault < ApplicationRecord
end
