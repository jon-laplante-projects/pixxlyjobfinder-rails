# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  attr_key               :string
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

class User < ApplicationRecord
	has_many :service_keys
	has_many :user_messages
	has_many :tokens
	has_many :incoming_messages
	has_and_belongs_to_many :listings
	has_many :listing_bookmarks
	has_many :bids
	has_many :filter_settings
	
  after_create :create_key
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  
  def create_key
    self.attr_key = Base64.encode64(SecureRandom.random_bytes(32)).delete("\n")
		# generate_token("attr_key", 64)
    self.save
  end
  
  def generate_token(column, length = 64)
	  begin
	    self[column] = Base64.encode64(SecureRandom.random_bytes(32)).delete("\n")
			# self[column] = SecureRandom.urlsafe_base64 length
      self[column] = self[column].gsub(/[^0-9a-z]/i, '').gsub(" ", "_").downcase
	  end while User.exists?(column => self[column])
  end
end
