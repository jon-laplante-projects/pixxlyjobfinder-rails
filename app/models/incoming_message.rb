# == Schema Information
#
# Table name: incoming_messages
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  service_id :integer
#  title      :string
#  body       :text
#  viewed     :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  identifier :string
#  sender     :string
#

require 'upwork/api'
require 'upwork/api/routers/messages'
require 'upwork/api/routers/organization/companies'
require 'upwork/api/routers/auth'

class IncomingMessage < ApplicationRecord
  belongs_to :user
  belongs_to :service
  
  def check(service_id, uid)
    case service_id
      # Reddit.
      when 1
        token = Token.where(service_id: service_id, user_id: uid).last
      
        # Check to see if we've already instanciated a session.
        session = session || nil
        unless session.class.to_s === "Redd::Models::Session"
          web = Redd::AuthStrategies::Web.new(user_agent: 'PixxlyJobFinder:v1.3.0',
              client_id:  Rails.application.secrets.reddit_key,
              secret:     Rails.application.secrets.reddit_secret,
              redirect_uri: Rails.application.secrets.callback_uri)

            client = Redd::APIClient.new(web)
          
          # Try to authenticate and if not, refresh the code.
          begin
            client.access = Redd::Models::Access.new(web, access_token: token.token)
            # client.authenticate(token.auth_code)
          rescue => e
           if e.message.to_s === "{\"error\": \"invalid_grant\"}"
             # If we have an invalid access grant, attempt to refresh the token.
             new_access = { access_token: token.token, refresh_token: token.refresh_code, expires_in: 9999 }
             client.access = Redd::Models::Access.new(web, new_access)
             
             # client.refresh(token.refresh_code)
             
             # Save the access token and refresh token.
              token.token = client.access.access_token
              token.refresh_code = client.access.refresh_token
              token.save
             
#              puts "Refreshing code."
        #    # puts "It's an invalid grant, all right."
           end
          end
        end

        params = {}
      
        begin
          session = Redd::Models::Session.new(client)
          
          msg = session.my_messages(category: 'unread', mark: false, **params)
        rescue => e
          msg = User.where(id: 0)
          
          # If we have a token that isn't working, delete it.
          unless token.blank?
            token.destroy
          end
        end
      
#         puts "Reddit: message count #{msg.count}."
        if msg.count > 0
          msg.each do |m|
            unless IncomingMessage.where(identifier: "#{m.name}").count > 0
              message = IncomingMessage.new
              message.user_id = uid
              message.service_id = service_id
              message.title = "#{m.subject}"
              message.body = "#{m.body}"
              message.identifier = "#{m.name}"
              message.sender = "#{m.author}"
              message.save
            end
          end
        end
    
      # Upwork
      when 2
        token = Token.where(service_id: service_id, user_id: uid).last
      
        puts "Upwork"
        config = Upwork::Api::Config.new({
          'consumer_key'    => "#{Rails.application.secrets.upwork_key}",
          'consumer_secret' => "#{Rails.application.secrets.upwork_secret}",
          'access_token'    => "#{token.token}",# assign if known
          'access_secret'   => "#{token.token_secret}",# assign if known
        })
      
        client = Upwork::Api::Client.new(config)
      
        companies = Upwork::Api::Routers::Organization::Companies.new(client)
        companies.get_list()
      
#         auth = Upwork::Api::Routers::Auth.new(client)
#         auth.get_user_info
      
        messages = Upwork::Api::Routers::Messages.new(client)
  
      # Twitter
      when 3
        token = Token.where(service_id: service_id, user_id: uid).last
      
#         puts "Twitter"
        unless token.blank?
          client = Twitter::REST::Client.new do |config|
            config.consumer_key        = "#{Rails.application.secrets.twitter_key}"
            config.consumer_secret     = "#{Rails.application.secrets.twitter_secret}"
            config.access_token        = "#{token.token}"
            config.access_token_secret = "#{token.token_secret}"
          end

          begin
            client.direct_messages_received.each do |dm|
              unless IncomingMessage.where(identifier: dm.id.to_s).count > 0
                message = IncomingMessage.new
                message.user_id = uid
                message.service_id = service_id
                message.body = "#{dm.text}"
                message.identifier = dm.id.to_s
                message.sender = "#{dm.sender.screen_name}"
                message.save
              end
            end
          rescue => err
            logger.info "IncomingMessage model error: #{err.message}"
            unless token.blank?
              token.destroy
            end
          end
        end

      # Freelancer
      when 4
        puts "4"

      # Guru
      when 5
        puts "5"

      # PeoplePerHour
      when 6
        puts "6"

      # Indeed
      when 7
        puts "7"
    end
  end
end
