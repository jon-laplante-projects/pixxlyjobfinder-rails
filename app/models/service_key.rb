# == Schema Information
#
# Table name: service_keys
#
#  id               :integer          not null, primary key
#  user_id          :integer
#  service_id       :integer
#  encrypted_uid    :string
#  encrypted_pwd    :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  encrypted_uid_iv :string
#  encrypted_pwd_iv :string
#

class ServiceKey < ApplicationRecord
  belongs_to :user
  
  cattr_accessor :attr_key
  
  attr_encrypted :uid, key: :attr_key
  attr_encrypted :pwd, key: :attr_key
end
