# == Schema Information
#
# Table name: listing_bookmarks
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  listing_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ListingBookmark < ApplicationRecord
  belongs_to :user
  has_many :listings
end
