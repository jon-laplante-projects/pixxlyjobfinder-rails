# == Schema Information
#
# Table name: services
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  bulkreply  :boolean
#

# 1 - reddit                                                                                                                                                 
# 2 - upwork                                                                                                                                                 
# 3 - twitter                                                                                                                                                
# 4 - freelancer                                                                                                                                             
# 5 - guru                                                                                                                                                   
# 6 - peopleperhour                                                                                                                                          
# 7 - indeed

require 'cgi'
require 'base64'
require 'openssl'

class Service < ApplicationRecord
  has_many :listings
  has_many :incoming_messages
  has_many :tokens
  
  def generate_signature(service, user_id, path)
    service_id = Service.select(:id).find_by(name: service)
    token = Token.where(service_id: service_id.id, user_id: user_id).last
    
    case service
      when "reddit"
        url = ""
        key = "#{Rails.application.secrets.reddit_key}"
      
      when "upwork"
        url = "https://www.upwork.com/api/#{path}"
        key = "#{Rails.application.secrets.upwork_key}"
      
      when "twitter"
        url = "https://api.twitter.com/oauth/#{path}"  
        key = "#{Rails.application.secrets.twitter_key}"
    end
    
    return sign(token.id, key, url)
  end
  
  def sign(id, consumer_key, url)
    id = "#{id}"
    oauth_consumer_key = "#{consumer_key}" # Dummy consumer key, change to yours
    oauth_nonce = Random.rand(100000).to_s
    oauth_signature_method = 'HMAC-SHA1'
    oauth_timestamp = Time.now.to_i.to_s
    oauth_version = '1.0'
    
    parameters = 'oauth_consumer_key=' +
      oauth_consumer_key +
      '&oauth_nonce=' +
      oauth_nonce +
      '&oauth_signature_method=' +
      oauth_signature_method +
      '&oauth_timestamp=' +
      oauth_timestamp +
      '&oauth_version=' +
      oauth_version
    
    base_string = 'GET&' + CGI.escape(url) + '&' + CGI.escape(parameters)

    ## Cryptographic hash function used to generate oauth_signature
    # by passing the secret key and base string. Note that & has
    # been appended to the secret key. Don't forget this!
    #
    # This line of code is from a SO topic
    # (http://stackoverflow.com/questions/4084979/ruby-way-to-generate-a-hmac-sha1-signature-for-oauth)
    # with minor modifications.
    secret_key = "p1xxlyj0bf1ndersecret" # Dummy shared secret, change to yours
    oauth_signature = CGI.escape(Base64.encode64("#{OpenSSL::HMAC.digest('sha1',secret_key, base_string)}").chomp)

    testable_url = CGI.escape(url) + '?' + parameters + '&oauth_signature=' + oauth_signature
    return testable_url

  end
end
