# == Schema Information
#
# Table name: bids
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  bid_at          :datetime
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  listing_id      :integer
#  subj            :string
#  message         :text
#  twitter_message :string
#  won_at          :datetime
#  message_id      :integer
#  message_type    :string
#

class Bid < ApplicationRecord
  belongs_to :user
  belongs_to :listing
  
  before_commit :apply
  
  def apply
    puts "Applying to position #{self.listing_id}."
    listing = Listing.find(self.listing_id)
    
    unless listing.blank?
      if listing.service.bulkreply === true
        
        case listing.service.name
          when "reddit"
            puts "Attempting Reddit."
            token = Token.where(service_id: listing.service.id, user_id: self.user_id).last
          
            web = Redd::AuthStrategies::Web.new(user_agent: 'PixxlyJobFinder:v1.3.0',
              client_id:  Rails.application.secrets.reddit_key,
              secret:     Rails.application.secrets.reddit_secret,
              redirect_uri: Rails.application.secrets.callback_uri)

            begin
            client = Redd::APIClient.new(web)
            # client.authenticate(token.auth_code)
            client.access = Redd::Models::Access.new(web, access_token: token.token)

            session = Redd::Models::Session.new(client)

            msg = session.user(listing.owner.to_s).send_message(subject: "#{self.subj}", text: "#{self.message}")
            rescue => err
              logger.info "\n================================================================\nError sending Reddit: #{err.message}\n================================================================"
            end

          when "twitter"
            puts "Attempting Twitter."
            token = Token.where(service_id: listing.service.id, user_id: self.user_id).last  
          
            client = Twitter::REST::Client.new do |config|
              config.consumer_key        = "#{Rails.application.secrets.twitter_key}"
              config.consumer_secret     = "#{Rails.application.secrets.twitter_secret}"
              config.access_token        = "#{token.token}"
              config.access_token_secret = "#{token.token_secret}"
            end

            begin
              client.create_direct_message(listing.owner.to_s, self.twitter_message, options = {})
            rescue => err
              logger.info "\n================================================================\nError sending Twitter: #{err.message}\n================================================================"
            end
        end
      end
    end
  end
  
#   def self.to_csv
#     CSV.generate do |csv|
#       csv << "bid_at, service, job_name, job_description, won_at"
#       Bid.all.each do |b|
#         csv << "#{b.bid_at}, #{b.listing.service.name}, #{b.listing.title}, #{b.listing.descr}, #{b.won_at}"
#       end
#     end
#   end
end
