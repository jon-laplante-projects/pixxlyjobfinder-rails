# == Schema Information
#
# Table name: listings
#
#  id         :integer          not null, primary key
#  service_id :integer
#  title      :string
#  descr      :text
#  url        :string
#  listed_at  :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  identifier :string
#  expired_at :datetime
#  owner      :string
#

class Listing < ApplicationRecord
  belongs_to :service
  belongs_to :listing_bookmark
  has_many :bids
  has_and_belongs_to_many :users
  
  def self.to_csv
    CSV.generate do |csv|
      csv << Listing.attribute_names
      Listing.all.each do |l|
        csv << l.attributes.values
      end
    end
  end
end
