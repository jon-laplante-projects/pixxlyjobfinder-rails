# == Schema Information
#
# Table name: user_messages
#
#  id                     :integer          not null, primary key
#  title                  :string
#  body                   :text
#  bid_message_default_id :integer
#  user_id                :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class UserMessage < ApplicationRecord
  belongs_to :user
end
