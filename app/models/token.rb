# == Schema Information
#
# Table name: tokens
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  service_id   :integer
#  auth_code    :string
#  refresh_code :string
#  token        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  verifier     :string
#  token_secret :string
#

class Token < ApplicationRecord
  belongs_to :user
  belongs_to :service
  
  # Exchange your oauth_token and oauth_token_secret for an AccessToken instance.
  def prepare_access_token(oauth_token, oauth_token_secret)
      consumer = OAuth::Consumer.new("#{Rails.application.secrets.twitter_key}", "#{Rails.application.secrets.twitter_secret}", { :site => "https://api.twitter.com/1.1", :scheme => :header })

      # now create the access token object from passed values
      token_hash = { :oauth_token => oauth_token, :oauth_token_secret => oauth_token_secret }
      access_token = OAuth::AccessToken.from_hash(consumer, token_hash )

      return access_token
  end
end
