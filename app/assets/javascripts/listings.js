/*******************************************************
TODO
[] pagination
*******************************************************/

var Listings = Listings || {};

Listings = {
  collection: [],
  
  filter_collection: function(){
    // Get the list of active services.
      var services = [];
      var replybulk = $('[name="bulkreply"]:checked').val();
    
    // This is kind of a goofy way to evaluate these, but it's what I've got now.
      var bulk, no_bulk;
    
      switch(replybulk){
        case "all":
          bulk = true;
          no_bulk = false;
        break;

        case "bulk":
          bulk = true;
          no_bulk = true;
        break;

        case "no_bulk":
          bulk = false;
          no_bulk = false;
        break;
      }
      
      $(".service-provider:checked").each(function(key, val){
        services.push($(val).attr("id"));
      });
    
    // Search text provided, searching title and post.
    if ( $("#search_text").val() != null && ($("#search_title_toggle").prop("checked") === true && $("#search_post_toggle").prop("checked") === true) || ($("#search_title_toggle").prop("checked") === false && $("#search_post_toggle").prop("checked") === false) ) {
      Listings.filtered = $.grep(Listings.collection, function(arr){ return services.includes(arr.service) && arr.bookmark === $("#bookmarked_only_toggle").prop("checked") && (arr.title.includes( $("#search_text").val() ) || arr.descr.includes( $("#search_text").val()) ) && (arr.bulkreply === bulk || arr.bulkreply === no_bulk) });
    }

    // Search text provided, searching title.
    if ( $("#search_text").val() != null && ($("#search_title_toggle").prop("checked") === true && $("#search_post_toggle").prop("checked") === false) ) {
      Listings.filtered = $.grep(Listings.collection, function(arr){ return services.includes(arr.service) && arr.bookmark === $("#bookmarked_only_toggle").prop("checked") && arr.title.includes( $("#search_text").val() ) && (arr.bulkreply === bulk || arr.bulkreply === no_bulk) });
    }

    // Search text provided, searching post.
    if ( $("#search_text").val() != null && ($("#search_title_toggle").prop("checked") === false && $("#search_post_toggle").prop("checked") === true) ) {
      Listings.filtered = $.grep(Listings.collection, function(arr){ return services.includes(arr.service) && arr.bookmark === $("#bookmarked_only_toggle").prop("checked") && arr.descr.includes( $("#search_text").val() && (arr.bulkreply === bulk || arr.bulkreply === no_bulk) ) });
    }
    
    // Only services and bookmarks, if there is no search text provided.
    if( $("#search_text").val() === null ){
      Listings.filtered = $.grep(Listings.collection, function(arr){ return services.includes(arr.service) && arr.bookmark === $("#bookmarked_only_toggle").prop("checked") && (arr.bulkreply === bulk || arr.bulkreply === no_bulk) });
    }
    
    // Check if the user wants to see bulk reply capable, non-bulk reply, or both.
      
      // Change displayed results to filtered while waiting on server.
      Listings.display(Listings.filtered);
  },
  
  filtered: [],
  
  display: function(data){
    if (data != null) {
      if(data.length > 0) {
        // Clear the display.
        $("#listings").html("");

        // Build the display.
        $.each(data, function(key, val){
          $("#listings").append( $("#listing_tmpl").html() );

          // Populate the service names.
          if (typeof val.bid_at === "undefined") {
            $("#listings").find(".listing-service").last().text(val.service);
          } else {
            $("#listings").find(".listing-service").last().html('<span class="fa fa-check-square-o"></span>&nbsp;' + val.service + ' - <small style="color: #777777;" class="bid-date">Bid made on ' + val.bid_at + '</small>');
          }

          // Flag the bookmarks.
          if (val.bookmark === true) {
            $("#listings").find(".bookmarkers").removeClass("fa-bookmark-o");
            $("#listings").find(".bookmarkers").addClass("fa-bookmark");
          }
          
          // Describe whether service supports bulk bids.
          if(val.bulkreply === true) {
            $("#listings").find(".bulk-supported").html("<span style='font-size: 8.5pt;' title='When you are bidding on jobs from " + val.service + ", you can create one message and send it to all bookmarks that support bulk applying.'>&nbsp;&bull;&nbsp;Supports bulk applying!</span>");
            $("#listings").find(".bulk-supported").css('textTransform', 'capitalize');
          }

          $("#listings").find(".listing-title").last().text(val.title);
          $("#listings").find(".listing-date").last().text(val.listed_at);
          $("#listings").find(".listing-desc").last().text(val.descr);
        });
      } else {
        $("#listings").html("<h2 style='text-align: center;'>There are no results that match your criteria.</h2>");
      }
    }
  },
  
  filter_remote: function(){
    // Get the list of active services.
    var services = [];

    $(".service-provider:checked").each(function(key, val){
      services.push($(val).attr("id"));
    });
    
    // Remote filtered data grab + update.
    var page = getUrlParam("page");
    if (page === null) {
      page = 1;
    }
    var payload = { search: $("#search_text").val(), title: $("#search_title_toggle").prop("checked"), post: $("#search_post_toggle").prop("checked"), offset: 0, bookmarks: $("#bookmarked_only_toggle").prop("checked"), services: services, bulkreply: $('[name="bulkreply"]').val(), page: page };

    $.ajax({
      url: '/listing/filter',
      type: 'POST',
      data: payload
    })
    .done(function(data) {
      
      if (data.success === true) {
        Listings.collection = data.listings;
        Listings.filtered = data.listings;
        Listings.display(data.listings);  
      } else {
        console.log(data.message);
      }
      
    })
    .fail(function(data) {
      console.log(data.message);
    })
  }
};

$(function(){
  /******************************
  // GET listings data
  ******************************/
  $.ajax({
    url: '/listings.json',
    type: 'GET',
    dataType: 'json'
  })
  .done(function(data) {
    Listings.collection = data;
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });

  /******************************
  // End listings data
  ******************************/
  
// Toggle bookmarks.
$(document).on("click", ".bookmarkers", function(){
  var that = this;
  var status = false;
  
  if ($(this).hasClass("fa-bookmark-o")) {
//     $(this).addClass("fa-bookmark");
//     $(this).removeClass("fa-bookmark-o");
    
    // Persist adding to bookmarked.
    status = true;
    
    // Swap the image to show it is active.
    $(this).siblings(".fa-bookmark").show();
    $(this).hide();
  } else {
//     $(this).addClass("fa-bookmark-o");
//     $(this).removeClass("fa-bookmark");

    // Persist removing from bookmarks.
    status = false;
    
    // Swap the image to show it is active.
    $(this).siblings(".fa-bookmark-o").show();
    $(this).hide();
  }
  
  var payload = { listing_id: $(that).parent().parent().data("id"), status: status };
  
  $.ajax({
      url: '/listing/bookmark',
      type: 'POST',
      data: payload
    })
    .done(function(data) {
      if (data.success === true) {
        console.log("Success!");
      } else {
        console.log(data.message);
      }
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
});
// Toggle bookmarks.  
  
  // Handle searching by bookmarks.
$("#bookmarks_nav_toggle_link").click(function(){
  // Update the UI.
  if ( $("#bookmarked_only_toggle").prop("checked") === true ) {
    $("#bookmarked_only_toggle").attr("checked", false);
    
    $("span#bookmark_nav_text").text(" Show Only Bookmarked");
  } else {
    $("#bookmarked_only_toggle").attr("checked", "checked");
    
    $("span#bookmark_nav_text").text(" Show All Listings");
  }

  $("#bookmarked_only_toggle").bootstrapToggle('toggle');
  
  // Push to server
  var page = $(".page.current").text().trim();
  var bookmarks_only = $("#bookmarked_only_toggle").prop("checked");

  // create dynamic element
  window.location = "/listings/?page=" + page + "&bookmarks=" + bookmarks_only;
//   $( "<a></a>", {
//     "style": "display: none;",
// //     "data-remote": "false",
//     "data-remote": "true",
//     "href": "/?page=" + page + "&bookmarks=" + bookmarks_only,
//     "on": {
//       // attach `click` event to dynamically created element
//       "click": function( event ) {
//         // Do something
//       }
//     }
//   }).appendTo( "body" )
//   // trigger `click` event on dynamically created element
//   .click()
//   .remove()
});
  
  
  /*******************************************************
  // Search and filter functionality.
  *******************************************************/
  // Clear search box.
  $(document).on("click", "#clear_search_btn", function(){
    $("#search_text").val("");
    
    var page = $(".page.current").text().trim();
    
    // create dynamic element
    window.location = "/listings/?page=" + page + "&clear_search=true";
//     $( "<a></a>", {
//       "style": "display: none;",
// //       "data-remote": "false",
//       "data-remote": "true",
//       "href": "/?page=" + page + "&clear_search=true",
//       // "href": "https://pixxlyjobfinder.com?page=" + page + "&clear_search=true",
//       // "href": "https://pixxlyjobfinder-rails-jonlaplante530707.codeanyapp.com?page=" + page + "&clear_search=true",
//       "on": {
//         // attach `click` event to dynamically created element
//         "click": function( event ) {
//           // Do something
//           // console.log(event.target, this.textContent)
//         }
//       }
//     }).appendTo( "body" )
//     // trigger `click` event on dynamically created element
//     .click()
//     .remove()
  });
  
  // Search for text.
  $(document).on("click", "#execute_search_btn", function(){
    var page = $(".page.current").text().trim();
    var search = $("#search_text").val();
    
    // create dynamic element
    window.location = "/listings/?page=" + page + "&search=" + search;
//     $( "<a></a>", {
//       "style": "display: none;",
// //       "data-remote": "false",
//       "data-remote": "true",
//       "href": "/?page=" + page + "&search=" + search,
//       // "href": "https://pixxlyjobfinder.com?page=" + page + "&search=" + search,
//       // "href": "https://pixxlyjobfinder-rails-jonlaplante530707.codeanyapp.com?page=" + page + "&search=" + search,
//       "on": {
//         // attach `click` event to dynamically created element
//         "click": function( event ) {
//           // Do something
//           // console.log(event.target, this.textContent)
//         }
//       }
//     }).appendTo( "body" )
//     // trigger `click` event on dynamically created element
//     .click()
//     .remove()
  });
  
  $("#search_text").keypress(function (e) {
   var key = e.which;
   if(key == 13)  // the enter key code
    {
      var page = $(".page.current").text().trim();
      var search = $("#search_text").val();

      // create dynamic element
      window.location = "/listings/?page=" + page + "&search=" + search;
//       $( "<a></a>", {
//         "style": "display: none;",
// //         "data-remote": "false",
//         "data-remote": "true",
//         "href": "/?page=" + page + "&search=" + search,
//         // "href": "https://pixxlyjobfinder.com?page=" + page + "&search=" + search,
//         // "href": "https://pixxlyjobfinder-rails-jonlaplante530707.codeanyapp.com?page=" + page + "&search=" + search,
//         "on": {
//           // attach `click` event to dynamically created element
//           "click": function( event ) {
//             // Do something
//             // console.log(event.target, this.textContent)
//           }
//         }
//       }).appendTo( "body" )
//       // trigger `click` event on dynamically created element
//       .click()
//       .remove()
    }
 });
  // End search/filter.
  
  /**************************************
  // Update the drawer settings.
  **************************************/
  $(document).on("click", ".update-filters", function(){
    var that = this;
    
    // Build vars.
    var page = $(".page.current").text().trim();
    var post = $("#search_post_toggle").prop("checked");
    var bulkreply = $('[name="bulkreply"]:checked').val();
    var title = $("#search_title_toggle").prop("checked");
    var bookmarks_only = $("#bookmarked_only_toggle").prop("checked");
    var services = [];
    
//     var sort_col, sort_direction;
//     if ( $(".sort-link").find("[data-sort-direction != 'none']").length > 0 ) {
//       sort_col = $(".sort-link").find("[data-sort-direction != 'none']").first().data("col");
//     } else {
//       sort_col = "";
//     }
    
//     if ( $(".sort-link").find("[data-sort-direction != 'none']").length > 0 ) {
//       sort_direction = $(".sort-link").find("[data-sort-direction != 'none']").first().data("sort-direction");
//     } else {
//       sort_direction = "";
//     }
    
    var sort_col =  $(that).data("col");
    var sort_direction = $(that).data("sort-direction");

    switch(sort_direction){
        case("none"):
          sort_direction = "asc";
        break;
        
        case("asc"):
          sort_direction = "desc";
        break;
        
        case("desc"):
          sort_direction = "asc";
        break
    }
    
    if (sort_direction === "none") {
      sort_direction = null;
    }

    $(".service-provider:checked").each(function(key, val){
      services.push($(val).attr("id"));
    });
    
    var search = $("#search_text").val();
    
    // Pass all vars in querystring and refresh the listings.
    window.location = '/listings/?page=' + page + '&services=' + services + "&bulkreply=" + bulkreply + "&post=" + post + "&title=" + title + "&bookmarks=" + bookmarks_only + "&sort_col=" + sort_col + "&sort_direction=" + sort_direction;
  });
  /**************************************
  // End update the drawer settings.
  **************************************/
  
  /**************************************
  // Expand a result in the listings table.
  **************************************/
  $(document).on("click", ".expand-rec", function(){
    $(this).siblings("a.collapse-rec").show();
    $(this).parent().parent().next("tr.tbl-content-row").show();
    $(this).hide();
  });
  /**************************************
  // End expand a result in the listings table.
  **************************************/
  
  /**************************************
  // Collapse a result in the listings table.
  **************************************/
  $(document).on("click", ".collapse-rec", function(){
    $(this).siblings("a.expand-rec").show();
    $(this).parent().parent().next(".tbl-content-row").hide();
    $(this).hide();
  });
  /**************************************
  // Collapse a result in the listings table.
  **************************************/
  
  /******************************
  // Handle pagination requests.
  ******************************/
//   $(document).on("click", ".page", function(event){
//     var that = this;
//     event.preventDefault();
//     event.stopPropagation();
  
//     /******************************
//     // Make the clicked link active.
//     ******************************/
//     $(".page").removeClass("current");
//     $(that).addClass("current");
    
//     /******************************
//     // Add/update querystring
//     // param with the page num.
//     ******************************/
//     var page_num = $(that).text().trim();
//     console.log("page_num = " + page_num);
//     history.pushState(null, null, '/listings?page=' + page_num);
    
//     // Do a local filter of the data.
//     Listings.filter_collection();

//     // Remote filtered data grab + UI update.
//     Listings.filter_remote();
//   });
});