var IncomingMessages = IncomingMessages || {};
var Notifications = Notifications || {};

setInterval(function(){
    getNotifications();
    updateBidLinks();
  },6000);

IncomingMessages = {
  collection: [],
  reddit: 0,
  upwork: 0,
  twitter: 0,
  count: function(){ return IncomingMessages.reddit + IncomingMessages.upwork + IncomingMessages.twitter }
}

Notifications = {
  current: 0
}

// Get querystring params.
function getUrlParam(name) {
  name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
  var results = regex.exec(location.search);
  return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

// Get messages/notifications from the server and update.
function getNotifications(){
  $.ajax({
    url: '/incoming_messages',
    type: 'GET',
    dataType: 'json'
  })
  .done(function(data) {
    var twitter_count = 0;
    var reddit_count = 0;

    $.each(data, function(key, val){
      if ( val.service === "reddit" ) {
        reddit_count++;
      }
      
      if ( val.service === "twitter" ) {
        twitter_count++;
      }
    });
    
    IncomingMessages.collection = data;
    IncomingMessages.reddit = reddit_count;
    IncomingMessages.twitter = twitter_count;
    
    $("#incoming_message_count").text(IncomingMessages.count());
    $("#reddit_message_count").text(reddit_count);
    $("#twitter_message_count").text(twitter_count);
    
    $("#message_replies_modal_body").html( buildNotifications(data) );
  })
  .fail(function() {
    console.log("Unable to retrieve messages.");  
  })
}

// Replace the bid link if a bid has been made today.
function updateBidLinks(){
  $.ajax({
        url: '/bids/today',
        type: 'GET',
      })
      .done(function(data) {
        
        if (data !== null) {
            $.each(data, function(key, val){
              $('small[data-id = "' + val + '"]').html('<small data-id="<%= listing.id %>">You have bid on this job.</small>');
            });
        } else {
          console.log(data.message);  
        }
        
      })
      .fail(function(data) {
        console.log(data.message);
      })
}

// Initialize...
$(function(){
  /**********************************
  // Populate IncomingMessages
  **********************************/
  getNotifications();
/************************************
// End incoming messages.
************************************/

// Init the message info buttons.
  $(".msg-info-btn").popover();
});

/******************************
// Functions...
******************************/
// Handle adding keywords to list...
function addKeywords(keywordType){
  event.preventDefault();
  var inputIdentifier, item, listIdentifier;

  if (keywordType === "search") {
    inputIdentifier = "#keyword_list_item";
    item = $(inputIdentifier).val();
    listIdentifier = "#list_items";

  } else {
    inputIdentifier = "#exclusion_list_item";
    item = $(inputIdentifier).val();
    listIdentifier = "#exclusion_list_items";

    console.log(item);
  }

  console.log(item);
  if(item)
  {
    $(listIdentifier).append("<li><input type='checkbox' checked/>" + item + "<a class='remove-keyword-x'>x</a><hr class='list-hr'></li>");
    $(inputIdentifier).val("");
  }
}

// Show or hide the services container div.
$(document).on("click", "#services_btn", function(){
$("#services_toggle_div").slideToggle("fast");
});

// When "All" is selected in the services container, nothing else is.
$("#service_all").on("change", function(){
  console.log("Tracking change");
  if ( $("#service_all").children().first().is(":checked") ) {
    console.log("Button on");
    $("#services_btns_grp>.btn.active").button("toggle");

    // Set the "All" option again, but delayed until other events can fire.
    setTimeout(function(){
      $("#service_all").button("toggle");
    }, 150);
  }
});

// When any other service is selected, "All" is toggled off.
$('#services_btns_grp>.btn').on("click", function(){
  if ( $("#service_all").children().first().is(":checked") ){
  }
});

$(document).on('change', '.checkbox', function()
{
  // changing checkbox state...
  if($(this).attr('checked'))
  {
    $(this).removeAttr('checked');
  }
  else
  {
    $(this).attr('checked', 'checked');
  }

  $(this).parent().toggleClass('completed');
});


/*******************************
// Process incoming messages
*******************************/
$(document).on("click", ".reply-on-reddit-btn", function(){
  var that = this;
  replyToMsg( $(that).data("id") );
});

// Process upwork messages.
$(document).on("click", ".reply-on-upwork-btn", function(){
  var that = $(this);
  $(this).parent().remove();

  IncomingMessages.upwork = IncomingMessages.upwork - 1;

  if (IncomingMessages.upwork !== 0) {
    $("#incoming_message_count").text(IncomingMessages.count());
    $("#upwork_message_count").text(IncomingMessages.upwork);
  } else {
    $("#incoming_message_count").text(IncomingMessages.count());
    $("#upwork_reply_panel").remove();
  }

  if (IncomingMessages.count() === 0) {
    $("#incoming_message_count").text("");
    $("#message_replies_modal_body").html('<ul class="list-group"><li class="list-group-item list-group-item-success"><span class="fa fa-thumbs-up"></span>&nbsp;No current messages.</li></ul>');
  }
});

// Process twitter messages.
$(document).on("click", ".reply-on-twitter-btn", function(){
  var that = this;
  replyToMsg( $(that).data("id") );
});

$(document).on("click", "#clear_all_reddit_btn", function(){
  IncomingMessages.reddit = 0;
  
  var payload = { service: 'reddit' };
  
  $.ajax({
        url: '/messages/viewed',
        type: 'POST',
        data: payload
      })
      .done(function(data) {
        
        if (data.success === true) {
          $("#reddit_reply_panel").remove();
          $("#incoming_message_count").text(IncomingMessages.count());
        } else {
          console.log(data.message);  
        }
        
      })
      .fail(function(data) {
        console.log(data.message);
      })

  if (IncomingMessages.count() === 0) {
    $("#incoming_message_count").text("");
    $("#message_replies_modal_body").html('<ul class="list-group"><li class="list-group-item list-group-item-success"><span class="fa fa-thumbs-up"></span>&nbsp;No current messages.</li></ul>');
  }
});

$(document).on("click", "#clear_all_upwork_btn", function(){
  IncomingMessages.upwork = 0;

  var payload = { service: 'upwork' };
  
  $.ajax({
        url: '/messages/viewed',
        type: 'POST',
        data: payload
      })
      .done(function(data) {
        
        if (data.success === true) {
          $("#upwork_reply_panel").remove();
          $("#incoming_message_count").text(IncomingMessages.count());
        } else {
          console.log(data.message);  
        }
        
      })
      .fail(function(data) {
        console.log(data.message);
      })

  if (IncomingMessages.count() === 0) {
    $("#incoming_message_count").text("");
    $("#message_replies_modal_body").html('<ul class="list-group"><li class="list-group-item list-group-item-success"><span class="fa fa-thumbs-up"></span>&nbsp;No current messages.</li></ul>');
  }
});

$(document).on("click", "#clear_all_twitter_btn", function(){
  IncomingMessages.twitter = 0;
  
  var payload = { service: 'twitter' };
  
  $.ajax({
        url: '/messages/viewed',
        type: 'POST',
        data: payload
      })
      .done(function(data) {
        
        if (data.success === true) {
          $("#twitter_reply_panel").remove();
          $("#incoming_message_count").text(IncomingMessages.count());
        } else {
          console.log(data.message);  
        }
        
      })
      .fail(function(data) {
        console.log(data.message);
      })

  if (IncomingMessages.count() === 0) {
    $("#incoming_message_count").text("");
    $("#message_replies_modal_body").html('<ul class="list-group"><li class="list-group-item list-group-item-success"><span class="fa fa-thumbs-up"></span>&nbsp;No current messages.</li></ul>');
  }
});
/*******************************
// End incoming messages
*******************************/

/*******************************
// Handle notifications
*******************************/
$(document).on("click", "#clear_notifications_btn", function(){
  Notifications.current = 0;
  
  if (Notifications.current === 0) {
    $("#notifications_count").text("");
  } else {
    $("#notifications_count").text(Notifications.current);
  }

  $("#notifications_modal_body").html('<li class="list-group-item list-group-item-success"><span class="fa fa-thumbs-up"></span>&nbsp;No new notifications.</li>');
});
/*******************************
// End notifications
*******************************/

/*******************************
// Copy to clipboard - init.
*******************************/
$(function(){
  new Clipboard('#clipboard_copy_btn');
});
/*******************************
// End copy to clipboard - init.
*******************************/


/******************************
// Bid report - view message details
******************************/
$(document).on("click", ".msg-info-btn", function(){
  var that = this;
  var message_type;
  
  switch( $(that).data("message-type") ){
    case "user":
      message_type = "User Created Template"
    break;
      
    case "custom":
      message_type = "Custom Message"
    break;
      
    case "template":
      message_type = "Default Template"
    break;
  }
  
  $("#bid_message_type").text( message_type );
  $("#bid_message_subj").text( $(that).data("subj") );
  $("#bid_message_body").text( $(that).data("body") );
  
//   if ( $(that).data("message-type") === "custom" ) {
//     $("#bid_message_details_footer").show();
//   } else {
//     $("#bid_message_details_footer").hide();
//   }
  
  $("#bid_message_details_modal").modal("show");
});
/******************************
// End details
******************************/

/******************************
// Reply to incoming message
******************************/
function replyToMsg(id){
  var payload = { message_id: id };
  
  $.ajax({
    url: '/message/viewed',
    type: 'POST',
    data: payload
  })
  .done(function(data) {    
    if (data.success === true) {
        getNotifications();
        buildNotifications(IncomingMessages.collection);
    } else {
      console.log(data.message);  
    }

  })
  .fail(function(data) {
    console.log(data.message);
  })
}
/******************************
// End incoming replies.
******************************/

/******************************
// Notifications builder
******************************/
function buildNotifications(data){
  var reddit = $.grep(data, function(msgs){ return msgs.service === "reddit" });
  var twitter = $.grep(data, function(msgs){ return msgs.service === "twitter" });
  var msgs_html = "";
  var reddit_html = "";
  var twitter_html = "";

  // if ( data.length > 0 ) {
    // Include reddit: [] and twitter: [] in the data.
     if ( reddit.length < 1 && twitter.length < 1 ) {
       msgs_html = `<div class="alert alert-success" role="alert"><span class="fa fa-thumbs-o-up"></span>&nbsp;No new messages.</div>`;
    } else {
      if ( reddit.length > 0 ) {
        reddit_html = `<div class="panel panel-default" id="reddit_reply_panel">
          <div class="panel-heading" id="message_reply_title"><button type="button" class="btn btn-xs btn-default pull-right" id="clear_all_reddit_btn">Clear All</button>Reddit&nbsp;<span class="badge" id="reddit_message_count">` + reddit.length + `</span></div>
          <div class="panel-body" id="reddit_reply_body">
            <ul class="list-group">`;

        // Iterate the Reddit rows and build out the code.
        $.each(reddit, function(key, val){
          reddit_html = reddit_html + `<li class="list-group-item"><a href="https://www.reddit.com/message/unread" target="_BLANK" class="btn btn-xs btn-default pull-right reply-on-reddit-btn" data-id="` + val.id + `">Reply on Reddit</a>` + val.title + `<br/><small>` + val.body + `</small></li>`;
        });

        reddit_html = reddit_html + `</ul></div></div>`;
      }

      // Iterate the Twitter rows and build out the code.
      if ( twitter.length > 0 ) {
        twitter_html = `<div class="panel panel-default" id="twitter_reply_panel"><div class="panel-heading" id="message_reply_title"><button type="button" class="btn btn-xs btn-default pull-right" id="clear_all_twitter_btn">Clear All</button>Twitter&nbsp;<span class="badge" id="twitter_message_count">` + twitter.length + `</span></div><div class="panel-body" id="message_reply_body"><ul class="list-group">`;

        $.each(twitter, function(key, val){
          twitter_html = twitter_html + `<li class="list-group-item"><a href="https://twitter.com/" target="_BLANK" class="btn btn-xs btn-default pull-right reply-on-twitter-btn" data-id="` + val.id + `">Reply on Twitter</a>` + val.title + `<br/><small>` + val.body + `</small></li>`;
        });

        twitter_html = twitter_html + `</ul></div></div>`;
    }
   }

    return msgs_html + reddit_html + twitter_html;
  }
// }
/******************************
// End notifications.
******************************/

/******************************
// Set up push drawer.
******************************/
  /* Set the width of the side navigation to 250px and the left margin of the page content to 250px */
  function openNav() {
    /* to toggle the sidebar, just switch the CSS classes */
    if( $("#sidebar").hasClass("collapsed") === true ){
      toggleDrawer();
    }
  }

  /* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
  function closeNav() {
    /* to toggle the sidebar, just switch the CSS classes */
    if( $("#sidebar").hasClass("collapsed") === false ){
      toggleDrawer();
    }
  }
/******************************
// End up push drawer.
******************************/

function toggleDrawer(){
  $("#sidebar").toggleClass("collapsed");
  $("#listings").toggleClass("col-md-8 col-md-10");
  
  if ( $("#sidebar").hasClass("collapsed") ) {
    window.localStorage.setItem("drawer_open", false);
  } else {
    window.localStorage.setItem("drawer_open", true);
  }
}

function managePin(that){
  if ( $(that).find("span").hasClass("fa-rotate-90") ) {
        $(that).find("span").removeClass("fa-rotate-90");
        
        // Save localStorage item for open drawer.
        window.localStorage.setItem("drawer_open", true);
        
      } else {
       $(that).find("span").addClass("fa-rotate-90");
        
        // Remote localstorage
        window.localStoragae.setItem("drawer_open", false);
      }
}

/******************************
// Fire events on page load.
******************************/
  $(function(){
    // Open or close the drawer, depending on the localStorage setting.
    if(window.localStorage.getItem("drawer_open") === "true"){ openNav() } else { closeNav() }
    
    // Time out flash notifications in 5 seconds.
    setTimeout(function(){
      $('.alert').slideUp("fast");
    }, 5000);
  });
/******************************
// End events on page load.
******************************/