$(function(){
  // Show the modal on load.
  $("#welcome_modal").modal("show");
  
  /**************************************
  // Send request to create accounts.
  **************************************/
  $("#create_service_accounts").click(function(){
    var services = [];
    
    // Build the services array.
    $("[name='services_account_signup']:checked").each(function(key, service){
      services.push($(service).val());
    });
    
    var payload = {services: services, uid: $("#services_signup_usr").val(), mail: $("#services_signup_email").val(), pwd: $("#services_signup_pwd").val()};
    $.ajax({
      url: 'service/accounts/new',
      type: 'POST',
      data: payload
    })
    .done(function(data) {
      if (data.success === true) {
        console.log("Successfully created service accounts.");
      } else {
        console.log(data.message);
      }
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
  });
});