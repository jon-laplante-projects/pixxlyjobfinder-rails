$("#save_reddit_credentials").click(function(){
  console.log("credentials...");
  var payload = {username: $("#reddit_uid").val(), password: $("#reddit_pwd").val(), service: 'reddit'};
  $.ajax({
      url: 'save_credentials',
      type: 'POST',
      data: payload
    })
    .done(function(data) {
      if (data.success === true) {
        console.log("success");
      } else {
        console.log(data.message);
      }
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
});

$("#bid_on_bookmarked_btn").click(function(){
  $.ajax({
        url: '/token_check',
        type: 'POST'
      })
      .done(function(data) {
        
        if (data.valid === true) {
          $("#job_bid_message_modal").modal("show");    
        } else {
          $("#token_modal").modal("show");
        }
        
      })
      .fail(function(data) {
        console.log(data.message);
      })
});