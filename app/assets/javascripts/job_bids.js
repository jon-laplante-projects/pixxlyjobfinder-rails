// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

// Set up button text, visible fields, button routing, etc. for the 'apply' dialog.
function init_apply_dialog(bulk, id){
  $("#job_bid_btn").data("listing-id", id);
  
  if (bulk === true) {
    $("#job_bid_btn").text("Bid Directly");
    $("#job_bid_btn").data("type", "direct");
    $("#clipboard_copy_btn").hide();
    $("#bid_subj_group").show();
    $("#bid_twitter_group").show();
  } else {
    $("#job_bid_btn").text("Bid at website");
    $("#job_bid_btn").data("type", "site");
    $("#clipboard_copy_btn").show();
    $("#bid_subj_group").hide();
    $("#bid_twitter_group").hide();
  }
}

// When an apply link is clicked, initialize the 'apply' dialog.
$(document).on("click", ".job-bid-message-link", function(){
  init_apply_dialog($(this).data("bulk"), $(this).data("id"));
});

// Count keystrokes for the Twitter field.
$(document).on("keyup", "#bid_message_twitter", function(){
  $("#bid_message_twitter_char_count").text($("#bid_message_twitter").val().length);
  
  if (parseInt( $("#bid_message_twitter").val().length ) > 140) {
    $("#bid_message_twitter_char_count").addClass("warning-red");
    $('label[for="bid_message_twitter"]').addClass("warning-red");
    $("#job_bid_btn").attr("disabled", "disabled");
  } else {
    $("#bid_message_twitter_char_count").removeClass("warning-red");
    $('label[for="bid_message_twitter"]').removeClass("warning-red");
    $("#job_bid_btn").attr("disabled", false);
  }
});

// Apply for all 'bulk apply' jobs that have been bookmarked and have NOT been applied to.
$("#bid_on_bookmarked_btn").click(function(){
  var bulk = true;
  init_apply_dialog(bulk, null);
});

// Handle "Bid Won" toggling.
$(document).on("click", ".bid-won-btn", function(){
  var that = this;
  var id = $(this).data("id");
  var payload = { bid_id: id };

  $.ajax({
        url: '/bid/won',
        type: 'POST',
        data: payload
      })
      .done(function(data) {
        
        if (data.success === true) {
          if ( data.toggle === "on" ) {
            $(that).parent().parent().find(".won-field").html("<span class='fa fa-trophy' title='" + data.won_at + "'></span>");
          } else {
            $(that).parent().parent().find(".won-field").html("");
          }
            
        } else {
          console.log(data.message);  
        }
        
      })
      .fail(function(data) {
        console.log(data.message);
      })
});

/******************************
// Handle job application
// activity.
******************************/
$(document).on("click", "#job_bid_btn", function(){
  var id = $(this).data("listing-id");
  
  $("#job_bid_message_modal").modal("hide");

  // Get the link to redirect to.
  var url = $('.listing-link[data-id="' + id + '"]').attr("href");

  // Build the Ajax payload.
  var payload = { listing_id: id, subj: $("#bid_message_subj").val(), message: $("#bid_message_text").val(), twitter: $("#bid_message_twitter").val(), message_id: $("#message_id").val(), message_type: $("#message_type").val() };

  $.ajax({
    url: '/log_bid',
    type: 'POST',
    data: payload
  })
  .done(function(data) {
    if (data.success === true) {
        // Reset the popup.
      $("#bid_message_twitter_char_count").text("0");
      $("#bid_message_twitter").val("");
      $("#bid_message_subj").val("");
      $("#bid_message_text").val("");
      $("#bid_message_twitter_char_count").removeClass("warning-red");
      $('label[for="bid_message_twitter"]').removeClass("warning-red");
      $("#job_bid_btn").attr("disabled", false);
      
      location.reload();

    } else {
      console.log(data.message);  
    }
  })
  .fail(function(data) {
    console.log(data.message);
  })
  
  // Is this a bulk-apply or single?
  if ($(this).data("type") === "site") {
    var win = window.open(url, '_blank');
    if (win) {
        //Browser has allowed it to be opened
        // Refresh the current page.
        location.reload();
      
        // Give the new page focus.
        win.focus();
    } else {
        //Browser has blocked it
        alert('Opening the job listing in a new tab has been blocked. Please allow popups for this site.');
    }
  } else {
    // Bulk apply.
    location.reload();
  }
});
/******************************
// End job application functionality.
******************************/