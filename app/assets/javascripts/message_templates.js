var Templates = Templates || {};

Templates = {
  collection: [],
  init_collection: function(target) {
    var url;
    if (target === "custom") {
      url = "/user_messages.json";
    } else {
      url = "/bid_message_defaults.json";
    }

    $.ajax({
        url: url,
        // url: '/bid_message_defaults.json',
        // url: 'assets/message_templates_test.json',
        type: 'GET',
        dataType: 'json'
      })
      .done(function(data) {
        // Add the templates collection to local memory.
        if (target === "custom") {
          Templates.usr_collection = data;
        } else {
          Templates.collection = data;
        }
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
  },
  usr_collection: [],
//   init_usr_collection: function() {
//     $.ajax({
//         url: '/user_messages.json',
//         // url: 'assets/message_templates_test.json',
//         type: 'GET',
//         dataType: 'json'
//       })
//       .done(function(data) {
//         // Add the templates collection to local memory.
//         Templates.usr_collection = data;
//       })
//       .fail(function() {
//         console.log("error");
//       })
//       .always(function() {
//         console.log("complete");
//       });
//   },
  current: function(id) {
    var currTemplate = $.grep(Templates.collection, function(coll) {
      return coll.id === id
    });

    return currTemplate[0];
  },
  usr_current: function(id) {
    var currTemplate = $.grep(Templates.usr_collection, function(coll) {
      return coll.id === id
    });

    return currTemplate[0];
  }
};
/********************************
// Retrieve message templates.
********************************/
Templates.init_collection("template");

/*******************************************************
// Retrieve user messages.
*******************************************************/
Templates.init_collection("custom");

// Let's preview a message.
$(document).on("click", ".preview-message-template", function() {
  // Retrieve the selected record.
  var current = Templates.current($(this).data("id"));

  // Set the preview values.
  $("#message_preview_title").html('<span class="fa fa-times close-message-preview pull-right"></span><span class="pull-right fa fa-pencil-square-o edit-message-template" data-id="' + current.id + '"></span>' + current.title);
  $("#message_preview_body").text(current.body);

  // Show/hide the appropriate dialogs.
  $("#message_templates_list").hide();
  $("#message_edit_panel").hide();
  $("#message_preview_panel").show();
});

// Close the message preview.
$(document).on("click", ".close-message-preview", function() {
  // Show/hide the appropriate dialogs.
  $("#message_templates_list").show();
  $("#message_edit_panel").hide();
  $("#message_preview_panel").hide();
});

// Let's edit a message.
$(document).on("click", ".edit-message-template", function() {

  // Set the edit values.
  var current = Templates.current($(this).data("id"));
  $("#message_edit_title_input").val(current.title);
  $("#message_editor_textarea").val(current.body);
  $("#save_cloned_template").data("template-id", $(this).data("id"));
  $("#message_edit_mode").val("new");

  // Show/hide the appropriate dialogs.
  $("#message_edit_panel").show();
  $("#message_templates_list").hide();
  $("#message_preview_panel").hide();
  
  // Update the collection.
  Templates.init_collection("template");
});

// Let's create a message.
$(document).on("click", ".create-usr-msg", function() {
  // Set the edit values.
  $("#message_edit_title_input").val();
  $("#message_editor_textarea").val();
  $("#save_cloned_template").data("template-id", "");
  $("#message_edit_mode").val("new");

  // Show/hide the appropriate dialogs.
  $("#message_edit_panel").show();
  $("#message_templates_list").hide();
  $("#message_preview_panel").hide();
  
  // Update the user collection.
  Templates.init_collection("custom");
});

// Edit an existing user message.
$(document).on("click", ".edit-usr-msg", function() {
  // Set the edit values.
  var current = Templates.usr_current($(this).data("id"));

  $("#message_edit_title_input").val(current.title);
  $("#message_editor_textarea").val(current.body);
  $("#save_cloned_template").data("template-id", $(this).data("id"));
  $("#message_edit_mode").val("edit");

  // Show/hide the appropriate dialogs.
  $("#message_edit_panel").show();
  $("#message_templates_list").hide();
  $("#message_preview_panel").hide();
  
  // Update the user collection.
  Templates.init_collection("custom");
});

// Clone and edit an existing user message.
$(document).on("click", ".clone-usr-msg", function() {
  // Set the edit values.
  var current = Templates.usr_current($(this).data("id"));
  $("#message_edit_title_input").val(current.title);
  $("#message_editor_textarea").val(current.body);
  $("#save_cloned_template").data("template-id", $(this).data("id"));
  $("#message_edit_mode").val("new");

  // Show/hide the appropriate dialogs.
  $("#message_edit_panel").show();
  $("#message_templates_list").hide();
  $("#message_preview_panel").hide();
  
  // Update the user collection.
  Templates.init_collection("custom");
});

// Delete user message.
$(document).on("click", ".del-usr-msg", function() {
  var that = this;

  var del = confirm("Are you sure you want to delete this custom message template?");

  if (del === true) {
    var payload = {
      id: $(this).data("id")
    };
    $.ajax({
        url: '/delete_custom_message',
        type: 'DELETE',
        data: payload
      })
      .done(function(data) {
        if (data.success === true) {
          console.log("deleted");

          $(that).parent().parent().parent().remove();
          $('.bid-message-items[data-id="' + $(that).data("id") + '"]').remove();
          
          // Update the usr_collection.
          Templates.init_collection("custom");
        } else {
          console.log(data.message);
        }

      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
  } else {

  }
});


// Save a cloned message.
$("#save_cloned_template").click(function() {
  var that = this;
  var payload = {
    title: $("#message_edit_title_input").val(),
    body: $("#message_editor_textarea").val(),
    template_id: $(this).data("template-id"),
    mode: $("#message_edit_mode").val()
  };

  $.ajax({
      url: '/custom_message',
      type: 'POST',
      data: payload
    })
    .done(function(data) {
      if (data.success === true) {
        var list_group_arr = [];
        var tmpl_group_arr = [];

        tmpl_group_arr.push("<a class='bid-message-items' data-id='" + data.id + "' data-remote='true' href='/user_messages/" + data.id + "'>");
        
        list_group_arr.push('<li class="list-group-item">');
        tmpl_group_arr.push('<li class="list-group-item">');
        
        list_group_arr.push('<span class="pull-right"><div class="btn-group" role="group">');
        list_group_arr.push('<button class="btn btn-sm btn-default clone-usr-msg" type="button" data-id="' + data.custom_msg.id + '" title="Copy & edit message"><span class="fa fa-clone highlighted-icon"></span></button>');
        list_group_arr.push('<button class="btn btn-sm btn-default edit-usr-msg" type="button" data-id="' + data.custom_msg.id + '" title="Edit message"><span class="fa fa-pencil-square-o highlighted-icon"></span></button>');
        list_group_arr.push('<button class="btn btn-sm btn-default del-usr-msg" type="button" data-id="' + data.custom_msg.id + '" title="Delete message"><span class="fa fa-minus highlighted-icon"></span></button>');
        list_group_arr.push('</div></span>');
        
        list_group_arr.push('<h4 class="leading">' + data.custom_msg.title + '</h4>');
        tmpl_group_arr.push('<h4 class="leading">' + data.custom_msg.title + '</h4>');
        
        list_group_arr.push('<small class="template-body">' + data.msg_truncated_body + '</small>');
        tmpl_group_arr.push('<small class="template-body">' + data.msg_truncated_body + '</small>');
        
        list_group_arr.push('</li>');
        tmpl_group_arr.push('</li>');
        
        tmpl_group_arr.push('</a>');

        // Let's reset mode, and update the UI if "edit" is true.
        if ($("#message_edit_mode").val() === "edit") {
          var msg = $('.clone-usr-msg[data-id=' + $(that).data("template-id") + ']');
          
          $(msg).parent().parent().parent().remove();
        }
        
        // Add the new record to the list.
        $("#message_templates_list").append(list_group_arr.join(""));
        $("#message_templates_list_bid").append(tmpl_group_arr.join(""));

        // Reset the mode input:hidden.
        $("#message_edit_mode").val("");
        
        // Show/hide the appropriate dialogs.
        $("#message_templates_list").show();
        $("#message_edit_panel").hide();
        $("#message_preview_panel").hide();
      } else {
        console.log(data.message);
      }

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
});
/********************************
// End message templates.
********************************/